<%--
  Created by IntelliJ IDEA.
  User: DELL
  Date: 8/2/2020
  Time: 21:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://www.springframework.org/tags" %>
<html>
<head>
    <title>Cảm ơn bạn !</title>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>Page Title</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>

    <link rel='stylesheet' type='text/css' media='screen' href='/resources/css/form/doneForm.css'>
<%--    <link rel='stylesheet' type='text/css' media='screen' href='responsive.css'>--%>

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>


    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;1,300;1,400" rel="stylesheet">
</head>
<body>
<input id="formID" type="hidden" value="${id}">
<div class="container">
    <div class="row">
        <div class="col-md-2 blank"></div>
        <div class="col-md-8 form-thank">
            <div class="response-done">
                <p class="font-title" id="title-form"></p>
                <p class="font-small">Câu trả lời của bạn đã được ghi lại.</p>
                <a class="font-small" href="/public/forms/detail/${id}">Gửi phản hồi khác</a>
            </div>
        </div>
        <div class="col-md-2 blank"></div>
    </div>
</div>
<script src="/resources/js/form/doneForm.js"></script>
</body>
</html>
