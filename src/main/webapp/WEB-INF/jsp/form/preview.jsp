<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<html>
<head>
    <title>Trả lời biểu mẫu</title>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" type="text/css" media="screen" href="/resources/css/form/style.css"/>
    <link rel="stylesheet" type="text/css" media="screen" href="/resources/css/form/responsive.css"/>
    <link rel="stylesheet" type="text/css" media="screen" href="/resources/css/form/mdtimepicker.css"/>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
    <!-- jQuery library -->
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="/resources/js/form/preview.js"></script>
</head>
<body>
<div class="container-fluid">
    <div class="form-content">
        <div class="imgform">
            <img id="img" alt="" id="0">
        </div>

        <input type="hidden" id="idForm" value="${id}">
        <div class="titleAndDescription">
            <label class="viewTitleForm"></label>
            <label class="viewDescriptionForm"></label>
        </div>

    </div>
    <!-- Button Submit -->
    <div class="container pagingForm">
        <div class="col-md-6 buttonProgress">
            <div class="btnPrevious">
                <a class="btn navbar-btn btn-previous">Quay lại</a>
            </div>
            <div class="btnNext">
                <a class="btn navbar-btn btn-next">Tiếp</a>
            </div>
        </div>
        <div class="col-md-6 progressPart">
            <progress id="progressPart" value="1" max=""></progress>
            <label class="showTextProgress">Trang <span class="currentNumberPart">1</span> trong tổng số <span
                    class="numberAllPart"></span></label>
        </div>
    </div>
    <div class="btnSubmit">
        <a class="btn navbar-btn btn-question">Gửi đi</a>
    </div>
</div>
<script type="text/javascript" src="/resources/js/form/ajaxFormDetail.js"></script>
<script type="text/javascript" src="/resources/js/form/answerUser.js"></script>
<script type="text/javascript" src="/resources/js/form/pagingForm.js"></script>
<script type="text/javascript" src="/resources/js/form/mdtimepicker.js"></script>
</body>
</html>
