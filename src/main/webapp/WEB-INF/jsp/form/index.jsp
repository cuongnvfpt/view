<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 7/17/2020
  Time: 9:17 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<html>
<head>
    <title>Biểu mẫu</title>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <script src="http://code.jquery.com/jquery-2.2.4.min.js"></script>
    <link rel="stylesheet" type="text/css" media="screen" href="/resources/css/form/style.css"/>
    <link rel="stylesheet" type="text/css" media="screen" href="/resources/css/form/responsive.css"/>
    <link rel="stylesheet" type="text/css" media="screen" href="/resources/css/form/media.css"/>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

    <!-- jQuery library -->
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="/resources/js/form/form.js"></script>
    <script src="/resources/js/form/media.js"></script>

</head>
<body>
<nav class="navbar navbar-style">
    <div class="container ctnHeader">
        <div class="navbar-header">

            <div class="menuToggle">
                <span class="burger"></span>
                <span class="burger"></span>
                <span class="burger"></span>
                <ul class="menu">
                    <a href="#">
                        <span class="glyphicon glyphicon-remove"></span>
                    </a>
                    <li style="padding-top: 40px;"><a style="color: #FDB813" href="/forms/edit/${id}">Câu hỏi</a></li>
                    <li><a href="/forms/report/${id}">Thống kê</a></li>
                    <li><a href="/logout">Đăng xuất</a></li>
                </ul>
            </div>


            <ul class="nav navbar-nav nav-listbtn">
                <li><a href="/forms/edit/${id}" class="btn navbar-btn btn-question">Câu hỏi</a></li>
                <li><a href="/forms/report/${id}" class="btn navbar-btn btn-result">Kết quả</a></li>
            </ul>

            <ul class="nav navbar-nav nav-listResponsive">
                <li>
                    <a class="eventAddQuestion">
                        <img src="/resources/img/form/addForm.jpg" alt="done" width="20" height="20">
                    </a>
                </li>
                <li>
                    <a href="#create" class="eventAddSection">
                        <img src="/resources/img/form/add2Form.jpg" width="20" height="20">
                    </a>
                </li>
                <li class="previewForm">
                    <a target="_blank">
                        <img src="/resources/img/form/viewForm.jpg" width="20" height="20">
                    </a>
                </li>
                <li class="imgCover">
                    <a href="#" data-toggle="modal" data-target="#myModal">
                        <img src="/resources/img/form/mediaForm.jpg" width="20" height="20">
                    </a>
                </li>
            </ul>

        </div>

        <div class="collapse navbar-collapse" id="nav1">
            <ul class="nav navbar-nav nav-list">
                <li>
                    <a class="eventAddQuestion" data-title="Thêm câu hỏi">
                        <img src="/resources/img/form/addForm.jpg" alt="done" width="20" height="20">
                    </a>
                </li>
                <li>
                    <a href="#create" class="eventAddSection" data-title="Thêm phần">
                        <img src="/resources/img/form/add2Form.jpg" width="20" height="20">
                    </a>
                </li>
                <li class="previewForm">
                    <a target="_blank" data-title="Xem trước">
                        <img src="/resources/img/form/viewForm.jpg" width="20" height="20">
                    </a>
                </li>
                <li class="imgCover">
                    <a href="#" data-toggle="modal" data-target="#myModal" data-title="Thêm ảnh bìa">
                        <img src="/resources/img/form/mediaForm.jpg" width="20" height="20">
                    </a>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a id="" class="btn btn-default navbar-btn btn-save create" style="display: none">Lưu</a>
                </li>
                <li>
                    <div class="dropdown">
                        <div class="dropdown-toggle" data-toggle="dropdown" id="srcImg">
                            <%--image fill by API--%>
                            <img class="user-boder" id="avt1"/>
                        </div>
                        <div class="dropdown-menu indexShowUser">
                            <b class="dropdown-title font-nomal" id="userName"></b>
                            <div class="divider"></div>
                            <div class="listTaskUser">
                                <a class="dropdown-item font-size-normal" data-toggle="modal" data-target="#myModalUser"
                                >Thay ảnh</a>

                                <a class="dropdown-item font-size-normal" href="/logout">Đăng xuất</a>
                            </div>
                        </div>
                    </div>

                </li>
            </ul>
        </div>
    </div>

</nav>

<%--Media--%>
<div class="container-fluid">
    <div class="container">
        <div class="media">

            <!-- Button to Open the Modal -->
            <!-- The Modal -->
            <div class="modal" id="myModal">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header">
                            <div class="col-sm-6 libraryPicture ">
                                <a class="text section isChoice" href="#" id="caa" onclick="myFunction1()">Thư viện
                                    ảnh</a>
                            </div>
                            <div class="col-sm-6 uploadPicture ">
                                <a class="text section" href="#" id="baa" onclick="myFunction()">Tải
                                    ảnh từ máy</a>
                            </div>
                            <button style="position: relative;margin-top: -35px; font-size: 30px; outline-style: none"
                                    type="button" class="close"
                                    data-dismiss="modal">&times;
                            </button>
                        </div>
                        <!-- Modal body -->
                        <div class="modal-body" id="Modal-Library">
                            <div id="default_img">

                            </div>
                        </div>
                        <div class="modal-body" id="Modal-Upload" hidden>
                            <form method="POST" enctype="multipart/form-data" id="fileUploadForm">
                                <input type="file" name="files" id="fileInputId2"
                                       accept="image/*;capture=camera"/><br/><br/>
                                <button type="button" id="btnSubmit" class="btn btn" data-dismiss="modal">Tải lên
                                </button>
                            </form>
                        </div>
                        <!-- Modal footer -->
                        <div class="modal-footer" id="my">
                            <button type="button" onclick="displayImage()" class="btn btn" data-dismiss="modal">Thêm
                            </button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


<div class="container-fluid">
    <div class="form-content">
        <div class="contentMessage"  id ="alert" style="display: none;">
            <div id ="alertContent" class="btn navbar-btn btn-question alertx" style="z-index: 1000;"></div>

        </div>

        <div class="contentMessageNew">


        </div>
        <div class="homePage">
            <a class="linkHomePage" href="/home">Trang chủ </a>
            <%--            <span class="titleShortDes">❯ Tiêu đề</span>--%>
        </div>


        <input type="hidden" id="idForm" value="${id}">
        <input type="hidden" id="countAnswers">
        <div class="imgform">
            <a class="btnRemoveImg" style="display:none;">
                <span class="glyphicon glyphicon-remove-circle imgX"></span>
            </a>
            <img id="img" alt="" id="0">
        </div>
        <div class="titleAndDescription">
            <div class="copyForm" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModalCopyForm"
                 data-title="Sao chép biểu mẫu">
                <span class="glyphicon glyphicon-duplicate"></span>
            </div>
            <input type="text" name="" id="title" class="title" placeholder="Tiêu đề khảo sát *" autocomplete="off">
            <input type="text" name="" id="description" class="description" placeholder="Thêm mô tả" autocomplete="off">
        </div>
        <input type="hidden" id="getIdForm" value="">
        <!-- Section 2 -->
        <div class="formSection isSelectSection" id="sectionId0">
            <input type="hidden" class="indexSection" value="0">
            <input type="hidden" class="getIdPart" id="getIdPart0" value="0">
            <div class="dropdown drdRemoveSection">
                <a class="dropdown-toggle" data-toggle="dropdown">
                    <span class="glyphicon glyphicon-option-vertical"></span>
                </a>
                <ul class="dropdown-menu drdMenu">
                    <li class="btnRemoveSection"><a>Xóa phần</a></li>
                </ul>
            </div>
            <div class="sectionNumber">
                <label>Phần <span class="countNumberSection"></span></label>
            </div>
            <input type="text" name="" class="titleFormSection" placeholder="Tiêu đề" id="titleFormSection0">
            <input type="text" name="" class="descriptionFormSection" placeholder="Thêm mô tả"
                   id="descriptionFormSection0">
            <!-- Question 1 -->
            <div class="container ctnQuestion">
                <div class="contentQuestion">
                    <a>
                        <span class="glyphicon glyphicon-option-vertical optionIconQuestion"></span>
                    </a>
                    <div class="eachQuestion">
                        <input type="hidden" class="typeQuestionId" value="1">
                        <input type="hidden" class="getIdQuestion" value="0" id="getIdQuestion00">
                        <div class=" flLeft">
                            <input type="text" name="" class="titleShortAnswer" placeholder="Đặt câu hỏi"
                                   id="titleShortAnswer0">
                            <div class="contentImgQuestion">
                                <div class="dropdown drdRemoveImage">
                                    <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        <span class="glyphicon glyphicon-option-vertical"></span>
                                    </a>
                                    <ul class="dropdown-menu drdMenu">
                                        <li class="btnRemoveQuestion"><a>Xóa ảnh</a></li>
                                    </ul>
                                </div>
                                <img class="imageQuestion" alt="" id="imageQuestion0">
                            </div>
                            <div class="typeAnswer">
                                <input type="text" name="" class="descriptionShortAnswer" placeholder="Đoạn trả lời"
                                       readonly="true">
                            </div>
                        </div>
                        <div class="flRight">
                            <div class="dropdown">
                                <button class="btn btn-primary dropdown-toggle btnTypeQuestion" type="button"
                                        data-toggle="dropdown">
                                    <span class="typeAnswerCurrent">Đoạn câu hỏi</span>
                                    <span class="glyphicon glyphicon-chevron-down"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    <li class="shortAnswert"><a>Đoạn câu hỏi</a></li>
                                    <li class="divider"></li>
                                    <li class="multipleChoice"><a>Trắc nghiệm</a></li>
                                    <li class="dropDownAnswer"><a>Menu thả</a></li>
                                    <li class="checkboxAnswer"><a>Hộp kiểm</a></li>
                                    <li class="divider"></li>
                                    <li class="checkboxAnswerGrid"><a>Lưới hộp kiểm</a></li>
                                    <li class="multipleChoiceGrid"><a>Lưới trắc nghiệm</a></li>
                                    <li class="divider"></li>
                                    <li class="dateAnswer"><a>Ngày</a></li>
                                    <li class="timeAnswer"><a>Giờ</a></li>
                                    <li class="divider"></li>
                                    <li class="addressAnswer"><a>Địa chỉ</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="controlQuestion col-md-12">
                        <ul>

                            <li><input class="myCheck" type="checkbox" value=""> Câu hỏi bắt buộc</li>
                            <li><a href="#" data-toggle="modal" data-target="#myModal"><span
                                    class="glyphicon glyphicon-picture"></span></a></li>

                            <li class="removeQuestion"><a></a><span class="glyphicon glyphicon-trash"></span></a>
                            </li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="myModalCopyForm" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Đặt tên mới cho biểu mẫu</h4>
                </div>
                <div class="modal-body">
                    <input type="text" class="form-control inputTitleCopy" placeholder="Nhập tên biểu mẫu">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-copyClose" data-dismiss="modal">Hủy</button>
                    <button type="button" id="copyForm" class="btn btn-default btn-copyDone create">Ok
                    </button>
                </div>
            </div>

        </div>
    </div>
    <!-- Button Submit -->
    <div class="btnSubmit">
        <a id="create" class="btn navbar-btn btn-question create">Tạo biểu mẫu</a>
    </div>

    <%--    user--%>

</div>


<div class="container">
    <!-- Button to Open the Modal -->
    <!-- The Modal -->
    <div class="modal fade" id="myModalUser">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h3 class="headerTitleAvatar">Thay đổi ảnh đại diện</h3>
                    <button type="button" class="close btn-close"
                            data-dismiss="modal">&times;
                    </button>
                </div>
                <!-- Modal body -->
                <div class="modal-body" id="Modal-Upload2">
                    <div class="col-sm-6 pull-left">
                        <div >
                            <%-- Cho xem trước ảnh đại diện ở chỗ này --%>
                            <img class="avatar" alt="" id="IMGavatar">
                        </div>
                    </div>
                    <div class="col-sm-6 pull-left ">
                        <form method="POST" enctype="multipart/form-data" id="fileUploadFormAVT">
                            <input type="file" name="files" onchange="mySubmit(this.form)" id="fileInputId"
                                   accept="image/*;capture=camera"/><br/><br/>
                        </form>
                    </div>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer modal-footerUser">
                    <div class="index-button">
                        <button class="btn btn-defaulf navbar-btn btn-cancel" data-dismiss="modal">Hủy</button>
                        <button class="btn btn-default navbar-btn btn-ok" id="btn-update-image">OK</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="alert1"></div>
<script type="text/javascript" src="/resources/js/main.js"></script>
<script type="text/javascript" src="/resources/js/form/insertForm.js"></script>
<script type="text/javascript" src="/resources/js/form/editFormDetail.js"></script>
<script type="text/javascript" src="/resources/js/home/profileUser.js"></script>
</body>
</html>
