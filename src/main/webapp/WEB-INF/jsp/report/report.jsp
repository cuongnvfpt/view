<%--
  Created by IntelliJ IDEA.
  User: DELL
  Date: 7/20/2020
  Time: 09:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Thống kê</title>

    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>Page Title</title>
    <meta name='viewport' content='width=device-width, initial-scale=1'>

    <link rel='stylesheet' type='text/css' media='screen' href='/resources/css/report/report.css'>

    <link rel='stylesheet' type='text/css' media='screen' href='/resources/css/report/responsiveReport.css'>

    <link rel='stylesheet' type='text/css' media='screen' href='/resources/css/form/style.css'>

    <link rel='stylesheet' type='text/css' media='screen' href='/resources/css/form/responsive.css'>

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>


    <!-- Latest compiled and minified CSS -->
    <%--    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">--%>

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>

    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>


    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;1,300;1,400" rel="stylesheet">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

    <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

</head>
<body>
<input type="hidden" id="idForm" value="${id}">
<nav class="navbar navbar-style">
    <div class="container ctnHeader">
        <div class="navbar-header">

            <div class="menuToggle">
                <span class="burger"></span>
                <span class="burger"></span>
                <span class="burger"></span>
                <ul class="menu">
                    <a href="#">
                        <span class="glyphicon glyphicon-remove"></span>
                    </a>
                    <li style="padding-top: 40px;"><a href="/forms/edit/${id}">Câu hỏi</a></li>
                    <li><a style="color: #FDB813" href="/forms/report/${id}">Thống kê</a></li>
                    <li>
                        <a id="deleteResultToggle" class="" style="display: block;">Xoá
                            kết quả</a>
                    </li>
                    <li><a href="/logout">Đăng xuất</a></li>
                </ul>
            </div>
            <ul class="nav navbar-nav nav-listbtn">
                <li><a href="/forms/edit/${id}" class="btn navbar-btn btn-question" id="btn-question">Câu hỏi</a></li>
                <li><a href="/forms/report/${id}" class="btn navbar-btn btn-result" id="btn-result">Kết quả</a></li>
            </ul>
        </div>

        <div class="collapse navbar-collapse" id="nav1">
            <ul class="nav navbar-nav navbar-right" id="nav-user">
                <li>
                    <a id="deleteResult" class="btn btn-default navbar-btn btn-save create" style="display: block;">Xoá
                        kết quả</a>
                </li>
                <li>                        <%--  --%>
                    <div class="dropdown">
                        <div class="dropdown-toggle" data-toggle="dropdown" id="srcImg">
                            <%--image fill by API--%>
                            <img class="user-boder" id="avt1"/>
                        </div>
                        <div class="dropdown-menu indexShowUser">
                            <b class="dropdown-title font-nomal" id="userName"></b>
                            <div class="divider"></div>
                            <div class="listTaskUser">
                                <a class="dropdown-item font-size-normal" data-toggle="modal"
                                   data-target="#myModalUser">Thay ảnh</a>
                                <a class="dropdown-item font-size-normal" href="/logout">Đăng xuất</a>
                            </div>
                        </div>
                    </div>

                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <ul class="list-question" id="list-question">
                <%--fill by API--%>
            </ul>
        </div>
        <div class="homePage-return">
            <a class="linkHomePage" href="/home">Trang chủ </a>
            <%--            <span class="titleShortDes">❯ Tiêu đề</span>--%>
        </div>
        <div class="col-md-7 main-div" id="main-div">
            <div class="box-main-title">
                <div class="content-title" id="content-title-form">
                    <%--fill by API--%>
                </div>
            </div>
            <div id="list-answers-question-detail">
                <%--fill by API--%>
            </div>

        </div>
        <div class="col-md-2"></div>
    </div>
</div>

<div class="container">
    <!-- Button to Open the Modal -->
    <!-- The Modal -->
    <div class="modal fade" id="myModalUser">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h3 class="headerTitleAvatar">Thay đổi ảnh đại diện</h3>
                    <button type="button" class="close btn-close"
                            data-dismiss="modal">&times;
                    </button>
                </div>
                <!-- Modal body -->
                <div class="modal-body" id="Modal-Upload">
                    <div class="col-sm-6 pull-left" style=" top: 26px; left: 27px;">
                        <div class="">
                            <%-- Cho xem trước ảnh đại diện ở chỗ này --%>
                            <img class="avatar" alt="" id="IMGavatar">
                        </div>
                    </div>
                    <div class="col-sm-6 pull-left " style="font-size: 16px;">
                        <form method="POST" enctype="multipart/form-data" id="fileUploadFormAVT">
                            <input type="file" name="files" onchange="mySubmit(this.form)" id="fileInputId"
                                   accept="image/*;capture=camera"/><br/><br/>
                        </form>
                    </div>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer modal-footerUser">
                    <div class="index-button">
                        <button class="btn btn-defaulf navbar-btn btn-cancel" data-dismiss="modal">Hủy</button>
                        <button class="btn btn-default navbar-btn btn-ok" id="btn-update-image">OK</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="/resources/js/report/reportJs.js"></script>
<script type="text/javascript" src="/resources/js/home/profileUser.js"></script>
<script type="text/javascript" src="/resources/js/main.js"></script>

<script>
    $(document).ready(function () {
        $(".menu").hide();
        $(".glyphicon-remove").click(function () {
            $(".menu").hide(300);
        });
        $(".burger").click(function () {
            $(".menu").show(300);
        });
    })
</script>
</body>
</html>
