<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<html>
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>login</title>
    <link rel="stylesheet" href="/resources/css/login/login.css" type="text/css"/>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"/>
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
<div class="login">
    <div class="block-login">
        <div class="block">
            <form id="formLogin" action="/j_spring_security" method="POST">
                <div class="imgContainer">
                    <img class="imgSize" src="/resources/img/login/vnpost_logo.png"/>
                </div>
                <div class="container font-size-normal">
                    <input type="text" placeholder="Tên đăng nhập" id="username" name="username" required/>
                    <input type="password" placeholder="Mật khẩu" id="myInput" data-toggle = "password" name="password" required/>
                    <a class="location-Views" onclick="myFunction()"><i class="fa fa-eye" id="eye"></i></a>
                    <button class="btnDN" type="submit" id="dangnhap">Đăng nhập</button>
                </div>
            </form>
            <c:if test="${ not empty message}">
                <div class="alert alert-danger alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <span> Account input false </span>
                </div>
            </c:if>
            <!-- Trigger the modal with a button -->
            <a class="btn btn-lg" data-toggle="modal" data-target="#myModal">Quên mật khẩu!</a>
            <!-- Modal -->
            <form   method="POST">
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                     aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Quên mật khẩu</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="form-group" id="messenger">
                                    <label for="email" class="col-form-label">Email:</label>
                                    <input type="text" class="form-control" id="email">
<%--                                    <br>--%>
<%--                                    <label style="color: #fdb813" id="notification">Please enter your email</label>--%>
<%--                                    <p style="color:crimson"> The system is sending gmail in <span id="countdowntimer"> 20 </span>--%>
<%--                                        Seconds</p>--%>
<%--                                    <label class="countdown" id="countdown"></label>--%>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                                    <button type="submit" id="btnsubmit" class="btn btn-primary">Gửi yêu cầu
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
<script type="text/javascript" src="/resources/js/login/login.js"></script>
<script type="text/javascript" src="/resources/js/login/loginSecurity.js"></script>
<script type="text/javascript" src="/resources/js/login/redirctLogin.js"></script>
</body>
</html>