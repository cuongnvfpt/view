<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" isELIgnored="false" %>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> Change Password </title>
    <link rel="stylesheet" href="/resources/css/login/changePassword.css" type="text/css"/>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"/>
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script type="text/javascript" src="/resources/js/login/change.js"></script>
</head>
<body>
<div class="changePassword">
    <div class="block-change">
        <div class="block">
            <form action="" method="post">
                <div class="text">
                    <h2>Tạo mới mật khẩu</h2>
                </div>
                <div class="container font-size-normal">
                    <input type="text" placeholder="Vui lòng nhập mật khẩu mới!" id="pass" name="password1" required/>
                    <input type="text" placeholder="Nhập lại mật khẩu mới!" id="myInput" name="password2"
                           required/>
                    <button class="btnDN" id="change" type="submit">OK</button>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>