<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>

<html>
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" type="text/css" media="screen" href="/resources/css/home/home.css"/>
    <link rel="stylesheet" type="text/css" media="screen" href="/resources/css/home/media.css"/>
    <link rel="stylesheet" type="text/css" media="screen" href="/resources/css/form/responsive.css"/>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"/>
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Trang chủ</title>
</head>
<%--
Phần profile User có sửa ở line 91 và import js ở line cuối
2 Kiên thay ảnh và load name User thì t có tạo file js riêng tên profileUser ở trong js home
 --%>
<body class="body" onload="asc(${typeS},${page})">
<!-- Header -->
<input type="hidden" id="search-option" value="${title}">
<div class="header">
    <div class="block">
        <div class="container">
            <nav class="navbar navbar-default" >
                <ul class="nav navbar-nav">
                    <!-- Dropdown -->

                    <li class="padding-img-vector imgButton">
                        <a class="nav-link font-size-normal" data-toggle="dropdown">
                                <img src="/resources/img/home/Vector.png"/>
                        </a>
                        <div class="dropdown-menu indexShow">
                                <a class="dropdown-item font-size-normal" href="/address/page/1/0">Sắp xếp A - Z</a>
                                <a class="dropdown-item font-size-normal" href="/address/page/2/0">Sắp xếp Z - A</a>
                                <a class="dropdown-item font-size-normal" href="/address/page/3/0">Gần đây</a>
                        </div>
                    </li>
                    <li class="padding-img-vector">
                        <img src="/resources/img/home/Vector.png"/>
                    </li>
                    <li class="menu dropdown border-dropdown">
                        <label role="button" class="nav-link dropdown-toggle font-size-normal"
                               data-toggle="dropdown" aria-expanded="false" aria-controls="ab">
                            <div  id="rename">
                            </div>
                        </label>
                        <div class="dropdown-menu indexShow" id="ab">
                            <a class="dropdown-item font-size-normal" href="/address/page/1/0">Sắp xếp A - Z</a>
                            <a class="dropdown-item font-size-normal" href="/address/page/2/0">Sắp xếp Z - A</a>
                            <a class="dropdown-item font-size-normal" href="/address/page/3/0">Gần đây</a>
                        </div>
                    </li>
                    <li class="padding-input-search">
                        <div class="nav navbar-nav navbar-right search">
                            <input type="text" class="search-text" id="search-text"  placeholder="Tìm kiếm">
                            <i typeof="submit" class="fa fa-search location-search-button"  id="search-icon"></i>
                        </div>
                    </li>
                </ul>

<%--                Nang tim cho nay       --%>
                <ul class=" nav navbar-nav navbar-right padding-avatar-user">
                    <div class="user-block">
                        <%--  --%>
                        <div class="" data-toggle="dropdown" id="srcImg">
                            <%--image fill by API--%>
                            <img class="user-boder" id="avt1"/>
                        </div>
                        <div class="dropdown-menu indexShowUser">
                            <b class="dropdown-title font-nomal" id="userName"></b>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item font-size-normal" data-toggle="modal" data-target="#myModalUser" href="/">Thay ảnh</a>
                            <a class="dropdown-item font-size-normal" href="/logout">Đăng xuất</a>
                        </div>
                    </div>
                </ul>
            </nav>
        </div>
<%-- Sua user o day nhe --%>

            <div class="container">
                <div class="media">
                    <!-- Button to Open the Modal -->
                    <!-- The Modal -->
                    <div class="modal fade" id="myModalUser">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <!-- Modal Header -->
                                <div class="modal-header">
                                    <h3 style="padding-left: 20px">Thay đổi ảnh đại diện</h3>
                                    <button  type="button" class="close btn-close"
                                             data-dismiss="modal">&times;
                                    </button>
                                </div>
                                <!-- Modal body -->
                                <div class="modal-body" id="Modal-Upload">
                                    <div class="col-sm-6 pull-left">
                                        <div >
                                            <%-- Cho xem trước ảnh đại diện ở chỗ này --%>
                                                <img class="avatar" alt="" id="IMGavatar" >
                                        </div>
                                    </div>
                                    <div class="col-sm-6 pull-left ">
                                        <form method="POST" enctype="multipart/form-data" id="fileUploadFormAVT" >
                                            <input type="file" name="files" onchange="mySubmit(this.form)" id="fileInputId" accept="image/*;capture=camera"/><br/><br/>
                                        </form>
                                    </div>
                                </div>
                                <!-- Modal footer -->
                                <div class="modal-footer">
                                    <div class="index-button">
                                        <button class="btn btn-defaulf navbar-btn btn-cancel" data-dismiss="modal">Hủy</button>
                                        <button class="btn btn-default navbar-btn btn-ok" id="btn-update-image">OK</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
    </div>
</div>
<!-- Home -->
<div class="home">
    <div class="container">
        <div class="contentMessage"  id ="alert" style="display: none;">
            <div id ="alertContent" class="btn navbar-btn btn-question alertx" style="z-index: 1000;"></div>
        </div>
        <div class="block" id="block">
            <div class="container">
                <div class="col-md-3 col-sm-6 pull-left block-menu">
                    <a href="/forms" class="text">
                        <div class="btn-new imgAdd">
                            <img class="imgSize" src="/resources/img/home/add.png"/>
                            <p class="font-size-normal font2">Tạo mới biểu mẫu</p>
                        </div>
                    </a>
                </div>
                <div id="bieumau">
                    <%-- One Form--%>
                    <div id="bieumau-element">
                    <%--fill by api--%>
                    </div>
                    <div id="notification" style="text-align: center"></div>
                </div>

            </div>
        </div>
    </div>
</div>
<%--paging--%>
<div class="container">
    <div class="container" style="display: flex; justify-content: center;">
        <div class="paging-container">
            <nav>
                <ul class="pagination" id="pagination" >
                    <li><a href="#" aria-label="Previous" id="previous-btn"><</a></li>
                    <div id="container-paging">
                        <div id="paging" class="pagination">
                        </div>
                    </div>
                    <li><a href="#" aria-label="Next" id="next-btn">></a></li>
                </ul>
            </nav>
        </div>
    </div>
</div>
<script src="/resources/js/home/ajaxForm.js"></script>
<script src="/resources/js/home/media.js"></script>
<script type="text/javascript" src="/resources/js/main.js"></script>
<script type="text/javascript" src="/resources/js/home/profileUser.js"></script>
</body>

</html>