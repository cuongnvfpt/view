function asc(typeS, page) {
    document.getElementById("bieumau-element").remove();
    var noti = document.getElementById("notification");
    noti.innerHTML = "";

    var urlAPI;
    var preUrl;
    var textSearch;
    var x = document.getElementById("rename");

    if (typeS == 1) {
        preUrl = "forms/ascending/";
        urlAPI = preUrl + page;
        x.innerHTML = "Sắp xếp A - Z";
    } else if (typeS == 2) {
        preUrl = "forms/descending/";
        urlAPI = preUrl + page;
        x.innerHTML = "Sắp xếp Z - A";
    } else if (typeS == 3) {
        preUrl = "forms/";
        urlAPI = preUrl + page;
        x.innerHTML = "Gần đây";
    } else if (typeS == 4) {
        var inputText = $("#search-option").val();
        if (!inputText == "") {
            $("#search-text").val(inputText);
            $("#search-option").val("");
        }
        var title = $("#search-text").val();
        preUrl = "forms/title?title=" + title + "&page=";
        urlAPI = preUrl + page;
        x.innerHTML = "Tìm kiếm";
    }
    $("#search-icon").click(function () {
        asc(4, 0);
    })

    $("#bieumau").append(`<div id="bieumau-element"></div>`);
    document.getElementById("paging").remove();
    //Paging
    if (typeS == 4) {
        textSearch = $("#search-text").val();
        $.ajax({
            headers: {"Authorization": 'Bearer ' + localStorage.getItem("eln_token")},
            url: "/formAPI/forms/numberpage?title=" + textSearch,
            dataType: 'json',
            contentType: "application/json",
            type: 'GET',
            success(numberPage) {
                $("#container-paging").append($("<div></div>", {
                    id: "paging",
                    class: "pagination",
                }));
                for (var i = 0; i < numberPage; i++) {
                    let liPaging = $("<li></li>");
                    if (page == i) {
                        let aPaging = $("<a></a>", {
                            html: (i + 1),
                            href: "/address/page/" + typeS + "/" + i + "?title=" + textSearch,
                            style: "background-color: #fdb813;",
                        });
                        $("#next-btn").attr("href", "/address/page/" + typeS + "/" + (i + 1) + "?title=" + textSearch);
                        if (i == numberPage - 1) {
                            $("#next-btn").css("pointer-events", "none");
                            $("#next-btn").css("opacity", "40%");
                        }
                        $("#previous-btn").attr("href", "/address/page/" + typeS + "/" + (i - 1) + "?title=" + textSearch);

                        if (i == 0) {
                            $("#previous-btn").css("pointer-events", "none");
                            $("#previous-btn").css("opacity", "40%");
                        }
                        liPaging.append(aPaging);
                    } else {
                        let aPaging = $("<a></a>", {
                            html: (i + 1),
                            href: "/address/page/" + typeS + "/" + i + "?title=" + textSearch,
                        });
                        liPaging.append(aPaging);
                    }
                    $("#paging").append(liPaging);
                }
            }
        })
    } else {
        $.ajax({
            url: "/formAPI/forms/numberpage",
            headers: {"Authorization": 'Bearer ' + localStorage.getItem("eln_token")},
            dataType: 'json',
            contentType: "application/json",
            type: 'GET',
            success(numberPage) {
                $("#container-paging").append($("<div></div>", {
                    id: "paging",
                    class: "pagination",
                }));
                for (var i = 0; i < numberPage; i++) {

                    let liPaging = $("<li></li>");
                    if (page == i) {
                        let aPaging = $("<a></a>", {
                            html: (i + 1),
                            href: "/address/page/" + typeS + "/" + i,
                            style: "background-color: #fdb813;",
                        });
                        $("#next-btn").attr("href", "/address/page/" + typeS + "/" + (i + 1));
                        if (i == numberPage - 1) {
                            $("#next-btn").css("pointer-events", "none");
                            $("#next-btn").css("opacity", "40%");
                        }
                        $("#previous-btn").attr("href", "/address/page/" + typeS + "/" + (i - 1));

                        if (i == 0) {
                            $("#previous-btn").css("pointer-events", "none");
                            $("#previous-btn").css("opacity", "40%");
                        }
                        liPaging.append(aPaging);
                    } else {
                        let aPaging = $("<a></a>", {
                            html: (i + 1),
                            href: "/address/page/" + typeS + "/" + i,
                        });
                        liPaging.append(aPaging);
                    }
                    $("#paging").append(liPaging);
                }
            }
        })
    }
    //Load avatar

    //Load form
    $.ajax({
        url: "/formAPI/" + urlAPI,
        headers: {"Authorization": 'Bearer ' + localStorage.getItem("eln_token")},
        dataType: 'json',
        contentType: "application/json",
        type: 'GET',
    }).done(function (result) {
        if (result === undefined) {
            noti.innerHTML = "Không tìm thấy biểu mẫu <a href='/home'>Ấn vào đây</a> để quay lại trang chủ";
            $("#next-btn").css("pointer-events", "none");
            $("#next-btn").css("opacity", "40%");
            $("#previous-btn").css("pointer-events", "none");
            $("#previous-btn").css("opacity", "40%");

        } else {
            result.forEach(item => {
                let div = ` 
                <!-- Modal Rename-->
                <div  class="modal fade" id="myModal` + item.id + `" role="dialog" xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html">
                    <div class="block-modal-dialog">
                    <div class="modal-dialog ">
                        <div class="modal-content size">
                            <div class="modal-header">
                                <h5 class="modal-title">Đổi tên mới cho biểu mẫu</h5>
                                </button>
                            </div>
                            <div class="modal-body">
                                <input type="text" id="edit-form` + item.id + `" required class="form-control" placeholder="Nhập tên biểu mẫu" value="` + item.title + `">
                                <br>
                            </div>
                            <div class="modal-footer">
                                <a class="btn btn-defaulf navbar-btn btn-cancel" data-dismiss="modal">Hủy</a>
                                <a class="btn btn-default navbar-btn btn-ok" onclick="editForm(` + item.id + `)">OK</a>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
                <!-- Modal Link  -->
                <div  class="modal fade" id="copyURL` + item.id + `" role="dialog" xmlns="http://www.w3.org/1999/html">
                    <div class="modal-copy-link">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Nhận đường dẫn liên kết</h4>
                                <button type="button" style="outline-style: none" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div class="modal-body">
                                <p class="font-nomal">Sao chép đường dẫn</p>
                                  <input id="url`+item.id+`" type = "text" class="show-link" readonly>
                            </div>
                            <div class="modal-footer">
                                <a data-dismiss="modal" class="btn btn-defaulf navbar-btn btn-cancel">Hủy</a>
                                <a data-toggle="modal" data-dismiss="modal" data-target="#myModal1` + item.id + `" onclick="copy(`+item.id+`)" class="btn btn-default navbar-btn btn-ok" >Sao chép</a>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
                <!-- Modal Success Copy URL -->
                <div  class="modal fade bd-example-modal-sm" id="myModal1` + item.id + `" role="dialog">
                    <div class="modal-dialog modal-sm copy-Url-Modal"  aria-labelledby="mySmallModalLabel" aria-hidden="true">
                        <div class="modal-content">
                            <div class="modal-header" style="justify-content: center">
                                <h5 class="modal-title" style="color: #144E8C">Đã sao chép liên kết</h5>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Form -->
                 <div  class="col-md-3 col-sm-6 pull-left block-menu" id="myDIV` + item.id + `">
                    <a  href="/forms/edit/` + item.id + `" class="text hrefEditImg`+item.id+`">
                        <div class="btn-form imgForm">
                            <div id="tat` + item.id + `">
                            <div class="imgSizeForm" >                               
                            </div>
                            <div class="copyLink" >
                                <a  onclick="copyLink(` + item.id + `)" data-toggle="modal" data-target="#copyURL` + item.id + `" id="save-link` + item.id + `" >
                                    <i class="fa fa-circle location-SaveLink2" style="text-shadow: 0px 4px 15px rgba(0, 0, 0, 0.1);"></i>
                                    <i class="fa fa-link location-SaveLink1"></i>
                                </a>
                            </div>
                            <div class="pull-left question-element">
                                <a href="/forms/edit/` + item.id + `" class="font-size-normal font1" id="hrefEditText`+item.id+`">` + item.title + `</a>
                            </div>
                            </div>
                            <div class="dropup pull-left ">
                                <div style="cursor: pointer" data-toggle="dropdown" class="location-imgMore" >
                                    <i class="fa fa-ellipsis-v"></i></div>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item font-size-normal btn btn-lg" data-toggle="modal" data-target="#myModal` + item.id + `">Đổi
                                        tên</a>
                                    <a class="dropdown-item font-size-normal" id="Tatkhaosat` + item.id + `" onclick="onOff(` + item.id + `)" href="#">Tắt khảo
                                        sát</a>
                                    <a class="dropdown-item font-size-normal deleteForm" style="cursor: pointer" onclick="deleteForm(` + item.id + `)">Xóa</a>
                                    <button onclick="copyLink(` + item.id + `)" id="unLink` + item.id + `" data-toggle="modal" data-target="#copyURL` + item.id + `" class="dropdown-item font-size-normal" href="#" >Sao chép liên kết</button>
                                    
                                </div>
                            </div>
                        </div>
                    </a>
                </div>`
                $("#bieumau-element").append(div);
                if (item.status == false) {
                    var element = document.getElementById("tat" + item.id);
                    element.classList.toggle("onOff");
                    var link = document.getElementById("save-link" + item.id);
                    link.classList.toggle("on");
                    var x = document.getElementById("Tatkhaosat" + item.id);
                    x.innerHTML = "Bật khảo sát";
                    document.getElementById("unLink" + item.id).disabled = true;
                }
                $('#myModal'+item.id).on('hidden.bs.modal', function (e) {
                    $('#edit-form' + item.id).val(item.title);
                })
            })
        }
    });
}

// Copy Link
function copyLink(idFormView) {
    document.getElementById("url" + idFormView).value = window.location.host + "/public/forms/detail/" + idFormView;
}

function copy(idFormView) {
    var copyText = document.getElementById("url" + idFormView);
    copyText.select();
    copyText.setSelectionRange(0, 99999); /*For mobile devices*/

    /* Copy the text inside the text field */
    document.execCommand("copy");
    setTimeout(function() {
        $('#myModal1'+idFormView).modal('hide');
    }, 2000); // milliseconds
}
// On/Off Form

function onOff(idFormEdit) {
    var element = document.getElementById("tat" + idFormEdit);
    var link = document.getElementById("save-link" + idFormEdit);
    element.classList.toggle("onOff");
    link.classList.toggle("on");
    var x = document.getElementById("Tatkhaosat" + idFormEdit);
    if (x.innerHTML === "Bật khảo sát") {
        x.innerHTML = "Tắt khảo sát";
        document.getElementById("unLink" + idFormEdit).disabled = false;
    } else {
        x.innerHTML = "Bật khảo sát";
        document.getElementById("unLink" + idFormEdit).disabled = true;
    }

    $.ajax({
        url: "/formAPI/forms/status/" + idFormEdit,
        dataType: 'json',
        contentType: "application/json",
        type: 'PUT',
        success(idFormEdit) {
        }
    });
}

// Edit
function editForm(idFormEdit) {
    var formEdit = {};
    formEdit["title"] = $("#edit-form" + idFormEdit).val();
    $.ajax({
        url: "/formAPI/forms/" + idFormEdit,
        dataType: 'json',
        contentType: "application/json",
        data: JSON.stringify(formEdit),
        type: 'PUT',
        success(idFormEdit) {
        }
    })
    window.location.reload();
}

// Delete Form
function deleteForm(idFormDelete) {
    var cf = confirm("Bạn chắc chắn xoá không ?");
    if (cf == true) {
        $.ajax({
            url: "/formAPI/forms/" + idFormDelete,
            dataType: 'json',
            contentType: "application/json",
            type: 'DELETE',
            success(response) {
                window.location.reload();
            },
            error(response) {
                window.location.reload();
                console.log(response)
            }
        })

    }
}
