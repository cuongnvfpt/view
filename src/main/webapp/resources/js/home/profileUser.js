$(document).ready(function () {
    $.ajax({
        type: "GET",
        headers: {"Authorization": 'Bearer ' + localStorage.getItem("eln_token")},
        url: "/formAPI/info/user",
        dataType: 'json',
        contentType: "application/json",
        success: function (data) {
            $("#avt1").attr({
                'src':data.avatar,
            })
            $("#srcImg2").append(`<img class="user-boder" src="`+data.avatar+`">`);
            $("#IMGavatar").attr({
                'src':data.avatar,
            })
            tempAVT = data.avatar;

            document.getElementById("userName").innerHTML = ""+ data.name;
        }
    })
});
var tempAVT = "";
$("#btn-update-image").on('click',function () {
   var srcEdit = {};
    srcEdit["avatar"] =  $('#IMGavatar').attr('src');
    $.ajax({
        type: "PUT",
        headers: {"Authorization": 'Bearer ' + localStorage.getItem("eln_token")},
        url: "/formAPI/change/newavartar",
        dataType: 'json',
        contentType: "application/json",
        data: JSON.stringify(srcEdit),
        success: function (data) {
            if($('#fileInputId').get(0).files.length !== 0) {
                $('#myModal').modal('hide');
                $('#myModalUser').modal('hide');
                $("#avt1").attr({
                    'src': data.avatar,
                })
                tempAVT = data.avatar,
                tempAlert1("Thay đổi ảnh thành công !!");
            }
            else{
                $('#myModal').modal('hide');
                $('#myModalUser').modal('hide');
                tempAlert1("Thay đổi ảnh thất bại !!");
            }
        },
        error: function (e) {
            alert("Thay đổi ảnh thất bại !!");
        }
    })
});

$('#myModal').on('hidden.bs.modal', function (e) {
    $("#fileInputId").val(null);
    $("#IMGavatar").attr({
        'src': tempAVT,
    })
});
$('#myModalUser').on('hidden.bs.modal', function (e) {
    $("#fileInputId").val(null);
    $("#IMGavatar").attr({
        'src': tempAVT,
    })
});
function tempAlert(msg,duration)
{
    var el = document.createElement("div");
    el.innerHTML = msg;
    el.className ="btn navbar-btn btn-question alertx";
    setTimeout(function(){
        el.parentNode.removeChild(el);
    },duration);
    document.body.appendChild(el);
}


