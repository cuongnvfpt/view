$.ajax({
    url: "/mediaAPI/city",
    dataType: 'json',
    contentType: "application/json",
    type: 'GET',
}).done(function (result) {
    result.forEach(item => {
        let a = $("<option></option>", {
            html: item.city,
            value: item.idCity,
        })
        $("#cars").append(a);
    })
});


$('#cars').on('change', function () {
    $('#huyen').find('option').remove().end()
        .append('<option value="-1">----Chon Huyen----</option>').val('-1');
    $('#xa').find('option').remove().end()
        .append('<option value="-1">----Chon Xa----</option>').val('-1');
    $.ajax({
        url: "/mediaAPI/district/" + this.value,
        dataType: 'json',
        contentType: "application/json",
        type: 'GET',
    }).done(function (result) {
        result.forEach(item => {
            let a = $("<option></option>", {
                html: item.districtName,
                value: item.idDistrict,
            })
            $("#huyen").append(a);
        })
    });
});


$('#huyen').on('change', function () {
    $('#xa').find('option').remove().end()
        .append('<option value="-1">----Chon Xa----</option>').val('-1');
    $.ajax({
        url: "/mediaAPI/commune/" + this.value,
        dataType: 'json',
        contentType: "application/json",
        type: 'GET',
    }).done(function (result) {
        result.forEach(item => {
            let a = $("<option></option>", {
                html: item.communeName,
                value: item.idPx,
            })
            $("#xa").append(a);
        })
    });
});
