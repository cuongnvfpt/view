google.charts.load('current', {'packages': ['corechart', 'bar']});

$(document).ready(function () {
    $("#btn-question").removeClass('btn-question');
    $("#btn-result").removeClass('btn-result');

    $("#btn-question").addClass('btn-result');
    $("#btn-result").addClass('btn-question');


    var id = $("#idForm").val();

    $.ajax({
        url: "/formAPI/forms/api/" + id,
        dataType: 'json',
        contentType: "application/json",
        type: 'GET',
        success(formItem) {
            let titleForm = $("<h2></h2>", {
                class: "font-title",
                html: formItem.title,
            })
            let shortDes = $("<p></p>", {
                class: "font-small",
                html: formItem.shortD,
            })

            let countAnswers = (`<p class="font-small">` + "Số lượt trả lời biểu mẫu : " + formItem.countAnswers + " lượt" + `</p>`)
            $("#content-title-form").append(titleForm);
            $("#content-title-form").append(shortDes);
            $("#content-title-form").append(countAnswers);

            let temp = formItem.title.split(" ");
            let titleShortDes;
            if (temp.length > 4) {
                titleShortDes = temp[0];
                for (let i = 1; i < 4; i++) {
                    titleShortDes = titleShortDes + ' ' + temp[i];
                }

                titleShortDes = titleShortDes + '...';
            } else {
                titleShortDes = formItem.title;
            }
            $('.homePage-return').append(`<span class="titleShortDes">❯ ` + titleShortDes + `</span>`);
            if (formItem.countAnswers == 0) {
                $('#deleteResult').on('click', function () {
                    alert("Không có kết quả để xoá !")
                });
                $('#deleteResultToggle').on('click', function () {
                    alert("Không có kết quả để xoá !")
                });
                $('#list-question').css("display", 'none');
                $('#main-div').css('margin-left', "320px");
                $('#list-answers-question-detail').css('display', 'none');
                $('#main-div').addClass('maginL');
                $('.homePage-return').css('margin-left', '22%');
                $('.ctnHeader').addClass('importantRule');
                $('.ctnHeader').removeClass('ctnHeader');
            } else {
                $('#nav-user').addClass('cssClassAdd');
                $('#deleteResult').on('click', function () {
                    var idForm = $('#idForm').val();
                    let cf = confirm('Bạn có chắc chắn xoá kết quả của biếu mẫu không');
                    if (cf) {
                        $.ajax({
                            url: "/formAPI/forms/deleteanswer/" + idForm,
                            dataType: 'json',
                            contentType: "application/json",
                            type: 'DELETE',
                            async: false,
                            success() {

                            }
                        })
                        window.location.reload();
                    }
                });

                $('#deleteResultToggle').on('click', function () {
                    var idForm = $('#idForm').val();
                    let cf = confirm('Bạn có chắc chắn xoá kết quả của biếu mẫu không');
                    if (cf) {
                        $.ajax({
                            url: "/formAPI/forms/deleteanswer/" + idForm,
                            dataType: 'json',
                            contentType: "application/json",
                            type: 'DELETE',
                            async: false,
                            success() {

                            }
                        })
                        window.location.reload();
                    }
                });

                //


                $.ajax({
                    url: "/formAPI/report/questionform/" + id,
                    timeout: 2500,
                    dataType: 'json',
                    contentType: "application/json",
                    type: 'GET',
                    async: false,
                    success(formItem) {
                        formItem.forEach((item, i) => {
                            //add question-element
                            let li = $("<li></li>", {
                                class: "question-element",
                            });
                            let a = $("<a></a>", {
                                class: "font-small",
                                html: item.question,
                                href: "#question-item-" + item.id,
                            });
                            li.append(a);
                            $("#list-question").append(li);

                            //add box-question
                            let boxQuestion = $("<div></div>", {
                                class: "box-question",
                                id: "question-item-" + item.id,
                            });
                            let titleQuestion = $("<p></p>", {
                                html: item.question,
                                class: "title-question font-nomal",
                            })
                            boxQuestion.append(titleQuestion);
                            $("#list-answers-question-detail").append(boxQuestion);
                        });
                    }
                })

                $.ajax({
                    url: "/formAPI/report/answers/" + id,
                    timeout: 1000,
                    dataType: 'json',
                    contentType: "application/json",
                    type: 'GET',
                    success(formItem) {
                        formItem.forEach((item) => {
                            if (item.idtypeshow == 1) {
                                let div = $("<div></div>");
                                let ul = $("<ul></ul>", {
                                    class: "list-answers",
                                    id: "list-answers-item-" + item.id,
                                });
                                div.append(ul);
                                $("#question-item-" + item.idquestion).append(div);

                                $.ajax({
                                    url: "/formAPI/report/answertext/" + item.id,
                                    dataType: 'json',
                                    contentType: "application/json",
                                    type: 'GET',
                                    success(itemElement) {
                                        if (itemElement === undefined) {
                                            $("#question-item-" + item.idquestion).append(`<div style="margin-left: 60px;padding-bottom: 15px;">Chưa có câu trả lời nào cho câu hỏi này !</div>`);
                                        } else {
                                            itemElement.forEach((itemE) => {
                                                if (itemE.countanswer != 1) {
                                                    let div = $("<div></div>");
                                                    let li = $("<li></li>", {
                                                        html: itemE.answer,
                                                        class: "answers-element",
                                                    });
                                                    let p = $("<p></p>", {
                                                        html: itemE.countanswer,
                                                        class: "count-number-answers"
                                                    });
                                                    div.append(li);
                                                    li.append(p);
                                                    $("#list-answers-item-" + itemE.idanswer).append(div);
                                                } else {
                                                    let div = $("<div></div>");
                                                    let li = $("<li></li>", {
                                                        html: itemE.answer,
                                                        class: "answers-element",
                                                    });
                                                    div.append(li);
                                                    $("#list-answers-item-" + itemE.idanswer).append(div);
                                                }
                                            });
                                        }
                                    }
                                });
                            } else if (item.idtypeshow == 2) {
                                let divChart = $("<div></div>", {
                                    class: "chart",
                                });
                                let divPieChart = $("<div></div>", {
                                    id: "pie-chart-item-" + item.id,
                                    class: "piechart",
                                });
                                divChart.append(divPieChart);
                                $("#question-item-" + item.idquestion).append(divChart);
                                $.ajax({
                                    url: "/formAPI/report/answerpiechart/" + item.id,
                                    dataType: 'json',
                                    contentType: "application/json",
                                    type: 'GET',
                                    success(itemElement) {
                                        var dataChart = [];

                                        if (itemElement === undefined) {
                                            $("#question-item-" + item.idquestion).append(`<div style="margin-left: 60px;padding-bottom: 15px;">Chưa có câu trả lời nào cho câu hỏi này !</div>`);
                                        } else {
                                            itemElement.forEach((itemE) => {
                                                var object = {};
                                                object["title"] = itemE.answer;
                                                object["number"] = itemE.countanswer;
                                                dataChart.push(object);
                                            });

                                            function drawChart() {
                                                var data = [
                                                    ['Task', 'Hours per Day']
                                                ];
                                                dataChart.forEach(element => {
                                                    data.push([element.title, element.number]);
                                                });

                                                var visualizedData = google.visualization.arrayToDataTable(data);
                                                // Optional; add a title and set the width and height of the chart
                                                var options = {
                                                    width: 600,
                                                    height: 400,
                                                    is3D: true,
                                                    legend: {
                                                        position: 'left'
                                                    },
                                                };
                                                // Display the chart inside the <div> element with id="piechart"
                                                var chart = new google.visualization.PieChart(document.getElementById('pie-chart-item-' + item.id));
                                                chart.draw(visualizedData, options);
                                            }

                                            google.charts.setOnLoadCallback(
                                                function () {
                                                    drawChart();
                                                });
                                        }
                                    }
                                });

                            } else if (item.idtypeshow == 3) {
                                let divChart = $("<div></div>", {
                                    class: "chart",
                                });
                                let divPieChart = $("<div></div>", {
                                    id: "col-chart-item-" + item.id,
                                    class: "colchart",
                                });
                                divChart.append(divPieChart);
                                $("#question-item-" + item.idquestion).append(divChart);

                                $.ajax({
                                    url: "/formAPI/report/answer/colchart/" + item.id,
                                    dataType: 'json',
                                    contentType: "application/json",
                                    type: 'GET',
                                    async: false,
                                    success(itemElement) {
                                        var data1 = [];
                                        itemElement.listCol.forEach(item => {
                                            var array = [item];
                                            itemElement.listColChart.forEach(listCol => {
                                                if (item == listCol.col) {
                                                    array.push(listCol.count);
                                                }
                                            });
                                            data1.push(array);
                                        });

                                        // console.log(data1.length);
                                        function drawChart() {
                                            var row = ['']
                                            var data = ([]);
                                            itemElement.listRow.forEach(item => {
                                                row.push(item);
                                            });
                                            data.push(row);
                                            //log
                                            data1.forEach(item => {
                                                data.push(item);
                                            });
                                            //log
                                            // console.log(data1)
                                            // console.log(data)
                                            var visualizedData = google.visualization.arrayToDataTable(data);
                                            var wid = (data1.length + row.length) * 200;
                                            var heg = (data1.length + row.length) * 22;
                                            var sumWH =data1.length + row.length;
                                            if (row.length < 10) {
                                                heg = 500;
                                            }
                                            if (sumWH< 7) {
                                                wid = 700;
                                            }
                                            if (sumWH > 7 && sumWH < 15) {
                                                wid = 1300;
                                            }
                                            console.log(row.length)
                                            console.log(data1.length)

                                            console.log(data1);
                                            var options = {
                                                width: wid,
                                                height: heg,
                                                legend: {
                                                    position: 'left'
                                                },
                                            };
                                            var chart = new google.charts.Bar(document.getElementById("col-chart-item-" + item.id));
                                            chart.draw(visualizedData, google.charts.Bar.convertOptions(options));
                                        }

                                        google.charts.setOnLoadCallback(
                                            function () {
                                                drawChart();
                                            });
                                    }
                                });
                            }
                        });
                    }
                })

                //
            }
        }
    })

});