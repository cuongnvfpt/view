$(document).ready(function () {
    $("#change").click(function (event) {
        event.preventDefault();
        // hàm
        test();
    });
});

function test() {
    var pass = $("#pass").val();
    var myInput = $("#myInput").val();
    if (pass == '') {
        alert("Nhập mật khẩu");
    } else if (myInput == '') {
        alert("Nhập lại mật khẩu");
    } else if (pass != myInput) {
        alert("Mật khẩu không tồn tại");
    } else if (pass == myInput && pass.length > 6 && pass.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/) && pass.match(/([0-9])/)) {
        NewPassword();
    } else {
        alert("Phải nhập ít nhất 6 kí tự, có chữ in hoa và số");
    }
}

function NewPassword() {
    var dataArray = {};
    dataArray["password"] = $("#pass").val();
    console.log(dataArray);
    $.ajax({
        type: "PUT",
        url: '/loginAPI/change/newpassword',
        headers: {"Authorization": 'Bearer ' + localStorage.getItem("eln_token")},
        dataType: 'json',
        contentType: "application/json",
        data: JSON.stringify(dataArray),
        success: function (data) {
            window.location.href = "/login";
        }
    });
}