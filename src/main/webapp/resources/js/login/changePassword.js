$(document).ready(function () {
    $("#changePassword").click(function (event) {
        event.preventDefault();
        test();
    });
});

function test() {
    var pass = $("#password1").val();
    var myInput = $("#myInput").val();
    if (pass == '') {
        alert("Nhập mật khẩu");
    } else if (myInput == '') {
        alert("Nhập lại mật khẩu");
    } else if (pass != myInput) {
        alert("Mật khẩu không tồn tại");
    } else if (pass == myInput && pass.length > 6 && pass.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/) && pass.match(/([0-9])/)) {
        setNewPassword();
    } else {
        alert("Phải nhập ít nhất 6 kí tự, có chữ in hoa và số");
    }
}

function setNewPassword() {
    var dataArray = {};
    dataArray["password"] = $("#password1").val();
    $.ajax({
        type: "PUT",
        url: '/loginAPI/web/resetpassword/'+$('#token_pass').val(),
        dataType: 'json',
        contentType: "application/json",
        data: JSON.stringify(dataArray),
        success: function (data) {
            if (data === true) {
                window.location = "/login";
            }
        }
    });
}