// $(document).ready(function () {
//        $("#dangnhap").click( function (event) {
//                event.preventDefault();
//                //hàm
//            testMatkhau();
//        });
// })
function testMatkhau() {
    var username2 = $("#username").val();
    $.ajax({
        type: "POST",
        url: "/loginAPI/web/testpassword/" + username2,
        dataType: 'json',
        contentType: "application/json",
        data: JSON.stringify(username2),
        success: function (data) {
            if (data === false) {
                window.location = "/changepassword";
            }
            if (data === true) {

                window.location = "/home";
            }
        }
    });
}

$('#dangnhap').on('click', function (e) {
    e.preventDefault();
    var formData = $('#formLogin').serializeArray();
    var data = {};
    $.each(formData, function (i, v) {
        data[v.name] = v.value;
    })
    //console.log(data);
    $.ajax({
        url: '/loginAPI/authenticate',
        type: 'POST',
        data: JSON.stringify(data),
        dataType: "json",
        contentType: "application/json",
        beforeSend: function () {
            $('body').css("pointer-events", "none");
            $('body').css("opacity", "0.4");
            $('body').append('<div class="spinner"></div>');
        },
        success: function (res) {
            localStorage.setItem("eln_token", res.accessToken);
            console.log(res);
            console.log(res.accessToken);
            $('#formLogin').submit();
            //let parseJwt1 = parseJwt(res.accessToken);
            //console.log(parseJwt1);
            // if(parseJwt1.first)
            // {
            //     // alert("true");
            //     window.location.href = "/home";
            // }else {
            //     // alert("false");
            //     window.location.href = "/changepassword";
            // }
        },
        error: function (res) {
            /* Swal.fire({
                 position: 'inherit',
                 icon: 'error',
                 title: "Sai mật khẩu hoặc tên tài khoản",
                 showConfirmButton: true
             });
             $('.loader').addClass("display-none");*/
            alert("Sai mật khẩu");
            window.location.href = "/login";
            console.log(res);
        }
    });
})
function parseJwt (token) {
    var base64Url = token.split('.')[1];
    var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    var jsonPayload = decodeURIComponent(atob(base64).split('').map(function(c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));
    return JSON.parse(jsonPayload);
};