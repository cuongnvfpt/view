$(document).ready(function () {
    $("#btnsubmit").click(function (event) {
        if($("#email").val() == ""){
            event.preventDefault();
            alert("Không được để trống email");
        }else {
            $("#notification").remove();
            console.log("1");
            event.preventDefault();
            $('#messenger').append(`<p id="notification">Hệ thống đang gửi đường dẫn đổi mật khẩu đến bạn <span id="spinner" class="spinner-border text-primary  spinner-border-sm"></span></p>`);
            $('#btnsubmit').css("pointer-events","none");
            sendmail();
        }
    });
    $('#myModal').on('hidden.bs.modal', function (e) {
        $('#email').val("");
        $("#notification").remove();
    })
});

function sendmail() {
    var name = $("#email").val();
    console.log(name);
    $.ajax({
        type: "POST",
        headers: {"Authorization": 'Bearer ' + localStorage.getItem("eln_token")},
        url: "/loginAPI/web/forgotpassword/" + name,
        dataType: 'json',
        contentType: "application/json",
        data: JSON.stringify(name),
        success: function (data) {
            if (data === false) {
                alert("Mail không phù hợp");
                $('#spinner').remove();
                $('#notification').append(`<span style="color: red">✘</span>`)
                $('#btnsubmit').css("pointer-events","auto");
            }
            else {
                $('#spinner').remove();
                $('#notification').append(`<span style="color: green">✔</span>`)
                $('#btnsubmit').css("pointer-events","auto");
            }
        }
    });
}

