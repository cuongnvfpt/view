function myFunction() {
    var x = document.getElementById("myInput");
    var y = document.getElementById("eye");
    if (x.type === "password") {
        x.type = "text";
        y.className = "fa fa-eye-slash";
    } else {
        x.type = "password";
        y.className = "fa fa-eye ";
    }
}
$('[data-toggle="password"]').password({
    append: 'right',        // can be left or right
    iconPrefix: 'fa',
    iconShow: 'fa-eye',
    iconHide: 'fa-eye-slash',
    tooltip: 'Show/Hide password',
    debug: true
});
