$(document).ready(function () {
    $('body').on('click', '.btnNext', function (e) {
        let numberPart = $('.formSection ').length;
        let currentPart = $('.partSelected').find('.numberIndexPart').val();

        let checkRe = true;
        let textContent;

        $('.partSelected').find('.eachQuestion').each(function (i) {
            $(this).parent().parent().css("border-color", "#144E8C");
            $(this).parent().parent().css("box-shadow", "none");
            let textContent;
            let checkRequired = $(this).find('.requiredShow').val();
            let typeQ = $(this).find('#typeOfAnswer').val();
            let resultPie = [];
            let resultRow = [];
            let resultCol = [];
            if (checkRequired == 'true') {
                if (typeQ == 1 || typeQ == 9) {
                    textContent = $(this).find('.resultText').val();
                    if (!textContent) {
                        checkRe = false;

                        $(this).parent().parent().css("border-color", "red");
                        $(this).parent().parent().css("box-shadow", "-5px 0px 0px rgba(255, 0, 0, 1)");
                    }
                }
                if (typeQ == 10) {
                    let hour = $(this).find('.hour').val();
                    let minute = $(this).find('.minute').val();

                    if (!hour || !minute) {
                        checkRe = false;

                        $(this).parent().parent().css("border-color", "red");
                        $(this).parent().parent().css("box-shadow", "-5px 0px 0px rgba(255, 0, 0, 1)");
                    }
                }
                if (typeQ == 11) {
                    let city = $(this).find('#city1 :selected').val();
                    let district = $(this).find('#district1 :selected').val();
                    let commune = $(this).find('#commune1 :selected').val();

                    if (city == -1 || district == -1 || commune == -1) {
                        checkRe = false;

                        $(this).parent().parent().css("border-color", "red");
                        $(this).parent().parent().css("box-shadow", "-5px 0px 0px rgba(255, 0, 0, 1)");
                    }
                }
                if (typeQ == 2) {
                    resultPie[0] = $(this).find('.checkResult:checked').val();

                    if (!resultPie[0]) {
                        checkRe = false;

                        $(this).parent().parent().css("border-color", "red");
                        $(this).parent().parent().css("box-shadow", "-5px 0px 0px rgba(255, 0, 0, 1)");
                    }
                }
                if (typeQ == 4) {
                    resultPie[0] = $(this).find('.viewDropdownMenu :selected').val();
                    if (!resultPie[0] || resultPie[0] == -1) {
                        checkRe = false;

                        $(this).parent().parent().css("border-color", "red");
                        $(this).parent().parent().css("box-shadow", "-5px 0px 0px rgba(255, 0, 0, 1)");
                    }
                }
                if (typeQ == 5) {
                    var val = [];
                    $(this).find(':checkbox:checked').each(function (i) {
                        val[i] = $(this).val();
                    });
                    resultPie = val;

                    if (resultPie.length == 0) {
                        checkRe = false;

                        $(this).parent().parent().css("border-color", "red");
                        $(this).parent().parent().css("box-shadow", "-5px 0px 0px rgba(255, 0, 0, 1)");
                    }
                }
                if (typeQ == 6) {
                    let rows = [];
                    let cols = [];
                    $(this).find('.resultCheckboxGrid:checked').each(function (i) {
                        rows[i] = $(this).val();

                        let indexRow = $(this).parent().find('.getIndexRow').val();

                        cols[i] = $(this).parent().parent().parent().find('.cols' + indexRow).text();


                    });
                    if (rows.length > 0) {
                        resultCol = cols;
                        resultRow = rows;
                    }
                    if (resultRow.length == 0) {
                        checkRe = false;

                        $(this).parent().parent().css("border-color", "red");
                        $(this).parent().parent().css("box-shadow", "-5px 0px 0px rgba(255, 0, 0, 1)");
                    }
                }
                if (typeQ == 8) {
                    let rowss = [];
                    let colss = [];
                    $(this).find('.resultRadioGrid:checked').each(function (i) {
                        rowss[i] = $(this).val();

                        let indexRow = $(this).parent().find('.getIndexRow').val();

                        colss[i] = $(this).parent().parent().parent().find('.cols' + indexRow).text();


                    });
                    if (rowss.length > 0) {
                        resultCol = colss;
                        resultRow = rowss;
                    }
                    if (resultRow.length == 0) {
                        checkRe = false;

                        $(this).parent().parent().css("border-color", "red");
                        $(this).parent().parent().css("box-shadow", "-5px 0px 0px rgba(255, 0, 0, 1)");
                    }
                }
            }
        });
        if (!checkRe) {
            alert("Xin hãy điền tất cả câu hỏi bắt buộc!");
            e.preventDefault();
        } else {
            if (currentPart < numberPart - 1) {
                $('.btnPrevious').css('display', 'block');

                $('#part' + currentPart).css('display', 'none');
                $('#part' + currentPart).removeClass('partSelected');

                let nextPart = parseInt(currentPart) + 1;
                $('#part' + nextPart).css('display', 'block');
                $('#part' + nextPart).addClass('partSelected');


                $('.currentNumberPart').text(nextPart + 1);
                $('#progressPart').attr('value', nextPart + 1);
            }
            if (currentPart == numberPart - 2) {
                $(this).css('display', 'none');
                $('.btnSubmit').css('display', 'block');
            }
        }
    });

    $('body').on('click', '.btnPrevious', function () {
        let numberPart = $('.formSection ').length;
        let currentPart = $('.partSelected').find('.numberIndexPart').val();
        console.log(currentPart + numberPart);

        if (currentPart >= 1) {
            $('.btnNext').css('display', 'block');
            $('.btnSubmit').css('display', 'none');

            $('#part' + currentPart).css('display', 'none');
            $('#part' + currentPart).removeClass('partSelected');

            let prePart = parseInt(currentPart) - 1;
            $('#part' + prePart).css('display', 'block');
            $('#part' + prePart).addClass('partSelected');

            $('.currentNumberPart').text(prePart + 1);
            $('#progressPart').attr('value', prePart + 1);

            if (currentPart == 1) {
                $(this).css('display', 'none');
            }
        }
    });
});