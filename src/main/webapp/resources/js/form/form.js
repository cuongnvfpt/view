$(document).ready(function () {
    $(".countNumberSection").each(function (i) {
        $(this).text(" " + (i + 1));
    });
    $(".countAnswerMulti").each(function (i) {
        $(this).text(" " + (i + 1));
    });
    $(".countFirstNumberDropMenu").each(function (i) {
        $(this).text(" " + (i + 1) + ". ");
    });
    $(".countAnswerCheckbox").each(function (i) {
        $(this).text(" " + (i + 1));
    });
    $(".countAnswerCheckboxGridRows").each(function (i) {
        $(this).text(" " + (i + 1) + ". ");
    });
    $(".countAnswerRadioGridRows").each(function (i) {
        $(this).text(" " + (i + 1) + ". ");
    });
    //-----------------
    $(".imageQuestion").each(function (i) {
        let check = $(this).attr('src');
        if (!check || check == "null") {
            $(this).parent().css('display', 'none');
        }
    });
    // show/menu bar responsive
    $(".menu").hide();
    $(".glyphicon-remove").click(function () {
        $(".menu").hide(300);
    });
    $(".burger").click(function () {
        $(".menu").show(300);
    });
    // Select section
    $('body').on('click', '.formSection', function () {
        $(".formSection").removeClass("isSelectSection");
        $(this).addClass("isSelectSection");
    });
    $('body').on('click', '.section', function () {
        $(".section").removeClass("isChoice");
        $(this).addClass("isChoice");
    });
    $('body').on('click', '.img1', function () {
        $(this).parent().parent().parent().find(".isSelect").remove();
        $(this).parent().append('<img  class="isSelect" src="/resources/img/media/check-o.svg">');
    });
    // Add new question to section selected
    $(".eventAddQuestion").click(function () {
        let partSelected = $('body').find('.isSelectSection').length;
        if (partSelected == 0) {
            alert("Hãy chọn phần bạn muốn thêm câu hỏi!");
        }
        $(".isSelectSection").append('<div class="container ctnQuestion"><div class="contentQuestion"><a><span class="glyphicon glyphicon-option-vertical optionIconQuestion"></span></a><div class="eachQuestion"><input type="hidden" class="typeQuestionId" value="1"><input type="hidden" class="getIdQuestion" value="0" id="getIdQuestion0"><div class=" flLeft"><input type="text" name="" class="titleShortAnswer" placeholder="Đặt câu hỏi"><div><div class="dropdown drdRemoveImage"><a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="glyphicon glyphicon-option-vertical"></span></a><ul class="dropdown-menu drdMenu"><li class="btnRemoveQuestion"><a>Xóa ảnh</a></li></ul></div><img class="imageQuestion" src="" alt=""></div><div class="typeAnswer"><input type="text" name="" class="descriptionShortAnswer" placeholder="Đoạn trả lời" readonly="true"></div></div><div class="flRight"><div class="dropdown"><button class="btn btn-primary dropdown-toggle btnTypeQuestion" type="button" data-toggle="dropdown"><span class="typeAnswerCurrent">Đoạn câu hỏi</span><span class="glyphicon glyphicon-chevron-down"></span></button><ul class="dropdown-menu"><li class="shortAnswert"><a>Đoạn câu hỏi</a></li><li class="divider"></li><li class="multipleChoice"><a>Trắc nghiệm</a></li><li class="dropDownAnswer"><a>Menu thả</a></li><li class="checkboxAnswer"><a>Hộp kiểm</a></li><li class="divider"></li><li class="checkboxAnswerGrid"><a>Lưới hộp kiểm</a></li><li class="multipleChoiceGrid"><a>Lưới trắc nghiệm</a></li><li class="divider"></li><li class="dateAnswer"><a>Ngày</a></li><li class="timeAnswer"><a>Giờ</a></li><li class="divider"></li><li class="addressAnswer"><a>Địa chỉ</a></li></ul></div></div></div><div class="controlQuestion col-md-12"><ul><li><input class="myCheck" type="checkbox" value=""> Câu hỏi bắt buộc</li><li><a href="#" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-picture"></span></a></li><li class="removeQuestion"><a></a><span class="glyphicon glyphicon-trash"></span></a></li></ul></div></div></div>');

        $('.imageQuestion').each(function (i) {
            $(this).attr('id', 'imageQuestion' + i);
        });

        //-----------------
        $(".imageQuestion").each(function (i) {
            let check = $(this).attr('src');
            if (!check || check == "null") {
                $(this).parent().css('display', 'none');
            }
        });

        $('.titleShortAnswer').each(function (i) {
            $(this).attr('id', 'titleShortAnswer' + i);
        });

        $('.isSelectSection').find('.ctnQuestion').each(function (i) {
            $(this).attr('id', 'ctnQuestion' + i);
        });

        let indexSection = $('.isSelectSection').find('.indexSection').val();
        $('.isSelectSection').find(".getIdQuestion").each(function (i) {
            $(this).attr('id', 'getIdQuestion' + indexSection + i);
        });
    });
    // Add new section
    $(".eventAddSection").click(function () {
        $('body').find('.isSelectSection').removeClass('isSelectSection');
        checkChangeForm = 1;
        $(".form-content").append('<div class="formSection isSelectSection"><input type="hidden" class="indexSection" value="0"><input type="hidden" class="getIdPart" id="getIdPart0" value="0"><div class="dropdown drdRemoveSection"><a class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-option-vertical"></span></a><ul class="dropdown-menu drdMenu"><li class="btnRemoveSection"><a>Xóa phần</a></li></ul></div><div class="sectionNumber"><label>Phần <span class="countNumberSection"></span></label></div><input type="text" name="" class="titleFormSection" placeholder="Tiêu đề" autocomplete="off"><input type="text" name="" class="descriptionFormSection" placeholder="Thêm mô tả" autocomplete="off"><!-- Question 1 --><div class="container ctnQuestion"><div class="contentQuestion"><a><span class="glyphicon glyphicon-option-vertical optionIconQuestion"></span></a><div class="eachQuestion"><input type="hidden" class="typeQuestionId" value="1"><input type="hidden" class="getIdQuestion" value="0" id="getIdQuestion0"><div class=" flLeft"><input type="text" name="" class="titleShortAnswer" placeholder="Đặt câu hỏi"><div><div class="dropdown drdRemoveImage"><a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="glyphicon glyphicon-option-vertical"></span></a><ul class="dropdown-menu drdMenu"><li class="btnRemoveQuestion"><a>Xóa ảnh</a></li></ul></div><img class="imageQuestion" src="" alt=""></div><div class="typeAnswer"><input type="text" name="" class="descriptionShortAnswer" placeholder="Đoạn trả lời" readonly="true"></div></div><div class="flRight"><div class="dropdown"><button class="btn btn-primary dropdown-toggle btnTypeQuestion" type="button" data-toggle="dropdown"><span class="typeAnswerCurrent">Đoạn câu hỏi</span><span class="glyphicon glyphicon-chevron-down"></span></button><ul class="dropdown-menu"><li class="shortAnswert"><a>Đoạn câu hỏi</a></li><li class="divider"></li><li class="multipleChoice"><a>Trắc nghiệm</a></li><li class="dropDownAnswer"><a>Menu thả</a></li><li class="checkboxAnswer"><a>Hộp kiểm</a></li><li class="divider"></li><li class="checkboxAnswerGrid"><a>Lưới hộp kiểm</a></li><li class="multipleChoiceGrid"><a>Lưới trắc nghiệm</a></li><li class="divider"></li><li class="dateAnswer"><a>Ngày</a></li><li class="timeAnswer"><a>Giờ</a></li><li class="divider"></li><li class="addressAnswer"><a>Địa chỉ</a></li></ul></div></div></div><div class="controlQuestion col-md-12"><ul><li><input class="myCheck" type="checkbox" value=""> Câu hỏi bắt buộc</li><li><a href="#" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-picture"></span></a></li><li class="removeQuestion"><a></a><span class="glyphicon glyphicon-trash"></span></a></li></ul></div></div></div></div>');

        $('.imageQuestion').each(function (i) {
            $(this).attr('id', 'imageQuestion' + i);
        });

        //-----------------
        $(".imageQuestion").each(function (i) {
            let check = $(this).attr('src');
            if (!check || check == "null") {
                $(this).parent().css('display', 'none');
            }
        });

        $(".countNumberSection").each(function (i) {
            $(this).text(" " + (i + 1));
        });

        $(".getIdPart").each(function (i) {
            $(this).attr('id', 'getIdPart' + i);
        });

        $('.form-content').find('.formSection').each(function (i) {
            $(this).attr('id', 'sectionId' + i);
            $(this).find('.titleFormSection').attr('id', 'titleFormSection' + i);
            $(this).find('.descriptionFormSection').attr('id', 'descriptionFormSection' + i);
            $(this).find('.indexSection').val(i);
        });

        let indexSection = $('.formSection').length - 1;
        $("#sectionId" + indexSection).find('.getIdQuestion').attr('id', 'getIdQuestion' + indexSection + '0');


        $('.titleShortAnswer').each(function (i) {
            $(this).attr('id', 'titleShortAnswer' + i);
        });

    });
    //remove class auto count answer
    $('body').on('change', '.countAnswerMulti', function () {
        $(this).removeClass("countAnswerMulti");
    });
    $('body').on('change', '.countSecondNumberDropMenu', function () {
        $(this).removeClass("countSecondNumberDropMenu");
    });
    $('body').on('change', '.countAnswerCheckbox', function () {
        $(this).removeClass("countAnswerCheckbox");
    });
    $('body').on('change', '.countInputCheckboxGridRows', function () {
        $(this).removeClass("countInputCheckboxGridRows");
    });
    $('body').on('change', '.countInputRadioGridRows', function () {
        $(this).removeClass("countInputRadioGridRows");
    });
    // Add new answer in question
    $('body').on('click', '#addAnswerRadio', function () {
        checkChangeForm = 1;
        $(this).parent().find(".answerMulti").append('<div><label class="labelDropdown"><input type="radio" disabled class="radioMulti" name="optradio"> <input type="text" name="" class="descriptionShortAnswer countAnswerMulti getOptionIndex" id="" value="1" onclick="this.select();"> <a class="btnRemove"><img src="/resources/img/form/closeForm.png" alt=""></a></label></div>');
        $(this).parent().find(".getOptionIndex").each(function (i) {
            $(this).parent().find('.countAnswerMulti').val("Tùy chọn " + (i + 1));
        });

        $('body').find('.getOptionIndex').each(function (i) {
            $(this).attr('id', 'getOptionIndex' + i);
        });
    });
    $('body').on('click', '#addAnswerDropdown', function () {
        checkChangeForm = 1;
        $(this).parent().find(".answerDropdown").append('<div><label class="labelDropdown"><span class="countFirstNumberDropMenu"></span><input type="text" name="" class="descriptionShortAnswer countSecondNumberDropMenu getOptionIndex" value="1" onclick="this.select();"> <a class="btnRemove"><img src="/resources/img/form/closeForm.png" alt=""></a></label></div>');
        $(this).parent().find(".countFirstNumberDropMenu").each(function (i) {
            $(this).text(" " + (i + 1) + ". ");
        });
        $(this).parent().find(".getOptionIndex").each(function (i) {
            $(this).parent().find('.countSecondNumberDropMenu').val("Tùy chọn " + (i + 1));

        });

        $('body').find('.getOptionIndex').each(function (i) {
            $(this).attr('id', 'getOptionIndex' + i);
        });
    });
    $('body').on('click', '#addAnswerCheckbox', function () {
        checkChangeForm = 1;
        $(this).parent().find(".answerMulti").append('<div><label class="labelDropdown"><input type="checkbox" disabled class="checkboxMulti" name="optradio"> <input type="text" name="" class="descriptionShortAnswer countAnswerCheckbox getOptionIndex" id="" value="1" onclick="this.select();"> <a class="btnRemove"><img src="/resources/img/form/closeForm.png" alt=""></a></label></div>');
        $(this).parent().find(".getOptionIndex").each(function (i) {
            $(this).parent().find('.countAnswerCheckbox').val("Tùy chọn " + (i + 1));
        });

        $('body').find('.getOptionIndex').each(function (i) {
            $(this).attr('id', 'getOptionIndex' + i);
        });
    });

    $('body').on('click', '#addAnswerCheckboxGridRows', function () {
        checkChangeForm = 1;
        $(this).parent().find(".answerCheckboxGridRows").append('<div><label class="labelDropdown"><span class="countAnswerCheckboxGridRows"></span> <input type="text" name="" class="descriptionShortAnswer countInputCheckboxGridRows getOptionRowsIndex rowS" id="indexOptionRows0" value="Tùy chọn 1" onclick="this.select();"> <a class="btnRemove"><img src="/resources/img/form/closeForm.png" alt=""></a></label></div>');
        $(this).parent().find(".countAnswerCheckboxGridRows").each(function (i) {
            $(this).text(" " + (i + 1) + ". ");
        });
        $(this).parent().find(".rowS").each(function (i) {
            $(this).parent().find(".countInputCheckboxGridRows").val("Tùy chọn " + (i + 1));
        });
        $(this).parent().find(".descriptionShortAnswer").each(function (i) {
            $(this).attr('id', 'indexOptionRows' + i);
        });

        $(this).parent().find(".descriptionShortAnswer").each(function (i) {
            $(this).attr('value', 'Tùy chọn ' + (i + 1));
        });
        var checkDupli = [];
        $(this).parent().find('.rowS').each(function (i) {
            checkDupli[i] = $(this).val();
        });
        for (let i = 0; i < checkDupli.length - 1; i++) {
            for (let j = i + 1; j < checkDupli.length; j++) {
                if (checkDupli[i] == checkDupli[j]) {
                    alert("Đã có hàng tương tự, xin hãy nhập lại!");
                    $(this).parent().find('#indexOptionRows' + j).select();
                }
            }
        }
    });
    $('body').on('click', '#addAnswerChecboxGridCols', function () {
        checkChangeForm = 1;
        $(this).parent().find(".answerChecboxGridCols").append('<div><label class="labelDropdown"><input type="checkbox" disabled class="checkboxMulti" name="optradio"> <input type="text" name="" class="descriptionShortAnswer countInputCheckboxGridRows getOptionColsIndex colS" id="indexOptionCols0" value="Tùy chọn 1" onclick="this.select();"> <a class="btnRemove"><img src="/resources/img/form/closeForm.png" alt=""></a></label></div>');
        $(this).parent().find(".colS").each(function (i) {
            $(this).parent().find(".countInputCheckboxGridRows").val("Tùy chọn " + (i + 1));
        });

        $(this).parent().find(".descriptionShortAnswer").each(function (i) {
            $(this).attr('id', 'indexOptionCols' + i);

        });

        $(this).parent().find(".descriptionShortAnswer").each(function (i) {
            $(this).attr('value', 'Tùy chọn ' + (i + 1));
        });

        var checkDupli = [];
        $(this).parent().find('.colS').each(function (i) {
            checkDupli[i] = $(this).val();
        });
        for (let i = 0; i < checkDupli.length - 1; i++) {
            for (let j = i + 1; j < checkDupli.length; j++) {
                if (checkDupli[i] == checkDupli[j]) {
                    alert("Đã có hàng tương tự, xin hãy nhập lại!");
                    $(this).parent().find('#indexOptionCols' + j).select();
                }
            }
        }
    });


    $('body').on('click', '#addAnswerRadioGridRows', function () {
        checkChangeForm = 1;
        $(this).parent().find("#answerRadioGridRows").append('<div><label class="labelDropdown"><span class="countAnswerRadioGridRows"></span><input type="text" name="" class="descriptionShortAnswer countInputRadioGridRows getOptionRowsIndex rowS" id="indexOptionRows0" value="Tùy chọn 1" onclick="this.select();"> <a class="btnRemove"><img src="/resources/img/form/closeForm.png" alt=""></a></label></div>');
        $(this).parent().find(".countAnswerRadioGridRows").each(function (i) {
            $(this).text(" " + (i + 1) + ". ");
        });
        $(this).parent().find(".rowS").each(function (i) {
            $(this).parent().find(".countInputRadioGridRows").val("Tùy chọn " + (i + 1));
        });
        $(this).parent().find(".descriptionShortAnswer").each(function (i) {
            $(this).attr('id', 'indexOptionRows' + i);
        });

        $(this).parent().find(".descriptionShortAnswer").each(function (i) {
            $(this).attr('value', 'Tùy chọn ' + (i + 1));
        });

        var checkDupli = [];
        $(this).parent().find('.rowS').each(function (i) {
            checkDupli[i] = $(this).val();
        });
        for (let i = 0; i < checkDupli.length - 1; i++) {
            for (let j = i + 1; j < checkDupli.length; j++) {
                if (checkDupli[i] == checkDupli[j]) {
                    alert("Đã có hàng tương tự, xin hãy nhập lại!");
                    $(this).parent().find('#indexOptionRows' + j).select();
                }
            }
        }
    });
    $('body').on('click', '#addAnswerRadioGridCols', function () {
        checkChangeForm = 1;
        $(this).parent().find("#answerRadioGridCols").append('<div><label class="labelDropdown"><input type="radio" disabled class="checkboxMulti" name="optradio"> <input type="text" name="" class="descriptionShortAnswer countInputCheckboxGridRows getOptionColsIndex colS" id="indexOptionCols0" value="Tùy chọn 1" onclick="this.select();"> <a class="btnRemove"><img src="/resources/img/form/closeForm.png" alt=""></a></label></div>');
        $(this).parent().find(".colS").each(function (i) {
            $(this).parent().find(".countInputCheckboxGridRows").val("Tùy chọn " + (i + 1));
        });
        $(this).parent().find(".descriptionShortAnswer").each(function (i) {
            $(this).attr('id', 'indexOptionCols' + i);
        });

        $(this).parent().find(".descriptionShortAnswer").each(function (i) {
            $(this).attr('value', 'Tùy chọn ' + (i + 1));
        });

        var checkDupli = [];
        $(this).parent().find('.colS').each(function (i) {
            checkDupli[i] = $(this).val();
        });
        for (let i = 0; i < checkDupli.length - 1; i++) {
            for (let j = i + 1; j < checkDupli.length; j++) {
                if (checkDupli[i] == checkDupli[j]) {
                    alert("Đã có hàng tương tự, xin hãy nhập lại!");
                    $(this).parent().find('#indexOptionCols' + j).select();
                }
            }
        }
    });
    // Remove section
    $('body').on('click', '.btnRemoveSection', function () {
        let numberPart = $('.formSection').length;
        if (numberPart > 1) {
            $(this).parent().parent().parent().remove();
            $(".countNumberSection").each(function (i) {
                $(this).text(" " + (i + 1));
            });
        } else {
            alert("Không thể xóa phần duy nhất!");
        }
    });
    // Remove question
    $('body').on('click', '.removeQuestion', function () {
        let checkLastQAllForm = $('.ctnQuestion').length;
        let checkLastQ = $(this).parent().parent().parent().parent().parent().find('.ctnQuestion').length;
        if (checkLastQ > 1) {
            $(this).parent().parent().parent().parent().remove();
        } else if (checkLastQAllForm > 1) {
            $(this).parent().parent().parent().parent().parent().remove();
            $(".countNumberSection").each(function (i) {
                $(this).text(" " + (i + 1));
            });
        } else {
            alert("Không thể xóa câu hỏi duy nhất!");
        }
    });
    // Remove image in question
    $('body').on('click', '.btnRemoveQuestion', function () {
        $(this).parent().parent().parent().find('.imageQuestion').attr('src', '');
        $(".imageQuestion").each(function (i) {
            let check = $(this).attr('src');
            if (!check || check == "null") {
                $(this).parent().css('display', 'none');
            }
        });

        $('.imageQuestion').each(function (i) {
            $(this).attr('id', 'imageQuestion' + i);
        })
    });
    // Event button remove
    $('body').on('click', '.btnRemove', function () {
        var bar = $(this).parent().parent().parent().parent();
        $(this).parent().parent().remove();
        // Multiple choice
        bar.find(".countAnswerMulti").each(function (i) {
            $(this).val("Tùy chọn " + (i + 1));
        });
        // Dropdown menu
        bar.find(".countFirstNumberDropMenu").each(function (i) {
            $(this).text(" " + (i + 1) + ". ");
        });
        bar.find(".countSecondNumberDropMenu").each(function (i) {
            $(this).val("Tùy chọn " + (i + 1));
        });
        // checkbox answer
        bar.find(".countAnswerCheckbox").each(function (i) {
            $(this).val("Tùy chọn " + (i + 1));

        });

        //  checkbox grid
        bar.find(".countAnswerCheckboxGridRows").each(function (i) {
            $(this).text(" " + (i + 1) + ". ");
        });
        bar.find(".rowS").each(function (i) {
            $(this).parent().find(".countInputCheckboxGridRows").val("Tùy chọn " + (i + 1));
        });
        bar.find(".rowS").each(function (i) {
            $(this).attr('id', 'indexOptionRows' + i);
        });

        // radio grid
        bar.find(".countAnswerRadioGridRows").each(function (i) {
            $(this).text(" " + (i + 1) + ". ");
        });
        bar.find(".colS").each(function (i) {
            $(this).parent().find(".countInputCheckboxGridRows").val("Tùy chọn " + (i + 1));
        });
        bar.find(".colS").each(function (i) {
            $(this).attr('id', 'indexOptionCols' + i);
        });

    });
    // Change type answer
    $('body').on('click', '.multipleChoice', function () {
        var i = 1;
        $(this).parent().parent().parent().parent().find(".typeAnswer").empty();
        $(this).parent().parent().parent().parent().find(".typeAnswer").append('<div class="answerMulti"><div><label class="labelDropdown"><input type="radio" disabled class="radioMulti" name="optradio"> <input type="text" name="" class="descriptionShortAnswer countAnswerMulti getOptionIndex" id="" value="Tùy chọn 1" onclick="this.select();"> <a class="btnRemove"><img src="/resources/img/form/closeForm.png" alt=""></a></label></div></div><input type="text" name="" class="moreOption" id="addAnswerRadio" placeholder="Thêm tùy chọn" readonly="true">');
        $(this).parent().parent().find(".typeAnswerCurrent").text('Trắc nghiệm');

        $(this).parent().parent().parent().parent().find(".typeQuestionId").val('2');

        $(this).parent().parent().parent().parent().find(".countAnswerMulti").text(" " + i);
    });
    $('body').on('click', '.shortAnswert', function () {
        $(this).parent().parent().parent().parent().find(".typeAnswer").empty();
        $(this).parent().parent().parent().parent().find(".typeAnswer").append('<input type="text" name="" class="descriptionShortAnswer" placeholder="Đoạn trả lời" readonly="true">');
        $(this).parent().parent().find(".typeAnswerCurrent").text('Đoạn câu hỏi');

        $(this).parent().parent().parent().parent().find(".typeQuestionId").val('1');
    });
    $('body').on('click', '.dropDownAnswer', function () {
        var i = 1;
        $(this).parent().parent().parent().parent().find(".typeAnswer").empty();
        $(this).parent().parent().parent().parent().find(".typeAnswer").append('<div class="answerDropdown"><div><label class="labelDropdown"><span class="countFirstNumberDropMenu"></span> <input type="text" name="" class="descriptionShortAnswer countSecondNumberDropMenu getOptionIndex" value="1" onclick="this.select();" id="getOptionIndex0"><a class="btnRemove"> <img src="/resources/img/form/closeForm.png" alt=""></a></label></div></div><input type="text" name="" class="moreOption" id="addAnswerDropdown" placeholder="Thêm tùy chọn" readonly="true">');
        $(this).parent().parent().find(".typeAnswerCurrent").text('Menu thả');

        $(this).parent().parent().parent().parent().find(".typeQuestionId").val('4');


        $(this).parent().parent().parent().parent().find(".countFirstNumberDropMenu").text(" " + i + ". ");
        $(this).parent().parent().parent().parent().find(".countSecondNumberDropMenu").val("Tùy chọn " + i);


        $('body').find('.getOptionIndex').each(function (i) {
            $(this).attr('id', 'getOptionIndex' + i);
        });
    });
    $('body').on('click', '.checkboxAnswer', function () {
        var i = 1;
        $(this).parent().parent().parent().parent().find(".typeAnswer").empty();
        $(this).parent().parent().parent().parent().find(".typeAnswer").append('<div class="answerMulti"><div><label class="labelDropdown"><input type="checkbox" disabled class="checkboxMulti" name="optradio"> <input type="text" name="" class="descriptionShortAnswer countAnswerCheckbox getOptionIndex" id="" value="Tùy chọn 1" onclick="this.select();"> <a class="btnRemove"><img src="/resources/img/form/closeForm.png" alt=""></a></label></div></div><input type="text" name="" class="moreOption" id="addAnswerCheckbox" placeholder="Thêm tùy chọn" readonly="true">');
        $(this).parent().parent().find(".typeAnswerCurrent").text('Hộp kiểm');

        $(this).parent().parent().parent().parent().find(".typeQuestionId").val('5');

        $(this).parent().parent().parent().parent().find(".countAnswerCheckbox").text(" " + i);
    });
    $('body').on('click', '.checkboxAnswerGrid', function () {
        var i = 1;
        $(this).parent().parent().parent().parent().find(".typeAnswer").empty();
        $(this).parent().parent().parent().parent().find(".typeAnswer").append('<div class="checkboxMultiGrid"><div class="col-md-5 col-xs-5 cMGRows"><Label>Hàng</Label><div class="answerCheckboxGridRows"><div><label class="labelDropdown"><span class="countAnswerCheckboxGridRows"></span> <input type="text" name="" class="descriptionShortAnswer countInputCheckboxGridRows getOptionRowsIndex rowS" id="indexOptionRows0" value="Tùy chọn 1" onclick="this.select();"> <a class="btnRemove"><img src="/resources/img/form/closeForm.png" alt=""></a></label></div></div><input type="text" name="" class="moreOption" id="addAnswerCheckboxGridRows" placeholder="Thêm tùy chọn" readonly="true"></div><div class="col-md-5 col-xs-5 cMGCols"><Label>Cột</Label><div class="answerChecboxGridCols"><div><label class="labelDropdown"><input type="checkbox" disabled class="checkboxMulti" name="optradio"> <input type="text" name="" class="descriptionShortAnswer countInputCheckboxGridRows getOptionColsIndex colS" id="indexOptionCols0" value="Tùy chọn 1" onclick="this.select();"> <a class="btnRemove"><img src="/resources/img/form/closeForm.png" alt=""></a></label></div></div><input type="text" name="" class="moreOption" id="addAnswerChecboxGridCols" placeholder="Thêm tùy chọn" readonly="true"></div>');
        $(this).parent().parent().find(".typeAnswerCurrent").text('Lưới hộp kiểm');

        $(this).parent().parent().parent().parent().find(".typeQuestionId").val('6');

        $(this).parent().parent().parent().parent().find(".countAnswerCheckboxGridRows").text(" " + i + ". ");
        $(this).parent().parent().parent().parent().find(".countInputCheckboxGridRows").val("Tùy chọn " + i);
    });
    $('body').on('click', '.multipleChoiceGrid', function () {
        var i = 1;
        $(this).parent().parent().parent().parent().find(".typeAnswer").empty();
        $(this).parent().parent().parent().parent().find(".typeAnswer").append('<div class="checkboxMultiGrid"><div class="col-md-5 col-xs-5 cMGRows"><Label>Hàng</Label><div id="answerRadioGridRows"><div><label class="labelDropdown"><span class="countAnswerRadioGridRows"></span> <input type="text" name="" class="descriptionShortAnswer countInputRadioGridRows getOptionRowsIndex rowS" id="indexOptionRows0" value="Tùy chọn 1" onclick="this.select();"> <a class="btnRemove"><img src="/resources/img/form/closeForm.png" alt=""></a></label></div></div><input type="text" name="" class="moreOption" id="addAnswerRadioGridRows" placeholder="Thêm tùy chọn" readonly="true"></div><div class="col-md-5 col-xs-5 cMGCols"><Label>Cột</Label><div id="answerRadioGridCols"><div><label class="labelDropdown"><input type="radio" disabled class="checkboxMulti" name="optradio"> <input type="text" name="" class="descriptionShortAnswer countInputCheckboxGridRows getOptionColsIndex colS" id="indexOptionCols0" value="Tùy chọn 1" onclick="this.select();"> <a class="btnRemove"><img src="/resources/img/form/closeForm.png" alt=""></a></label></div></div><input type="text" name="" class="moreOption" id="addAnswerRadioGridCols" placeholder="Thêm tùy chọn" readonly="true"></div></div>');
        $(this).parent().parent().find(".typeAnswerCurrent").text('Lưới trắc nghiệm');

        $(this).parent().parent().parent().parent().find(".typeQuestionId").val('8');


        $(this).parent().parent().parent().parent().find(".countAnswerRadioGridRows").text(" " + i + ". ");
        $(this).parent().parent().parent().parent().find(".countInputRadioGridRows").val("Tùy chọn " + i);
        $(this).parent().parent().parent().parent().find(".countInputCheckboxGridRows").val("Tùy chọn " + i);
    });
    $('body').on('click', '.dateAnswer', function () {
        $(this).parent().parent().parent().parent().find(".typeAnswer").empty();
        $(this).parent().parent().parent().parent().find(".typeAnswer").append('<div><input type="text" name="" class="descriptionShortAnswer" placeholder="Ngày, tháng, năm" readonly="true"><span class="glyphicon glyphicon-calendar"></span></div>');
        $(this).parent().parent().find(".typeAnswerCurrent").text('Ngày');
        $(this).parent().parent().parent().parent().find(".typeQuestionId").val('9');
    });
    $('body').on('click', '.timeAnswer', function () {
        $(this).parent().parent().parent().parent().find(".typeAnswer").empty();
        $(this).parent().parent().parent().parent().find(".typeAnswer").append('<div><input type="text" name="" class="descriptionShortAnswer" placeholder="Giờ" readonly="true"><span class="iconTimeEdit glyphicon glyphicon-time"></span></div>');
        $(this).parent().parent().find(".typeAnswerCurrent").text('Giờ');
        $(this).parent().parent().parent().parent().find(".typeQuestionId").val('10');
    });
    $('body').on('click', '.addressAnswer', function () {
        $(this).parent().parent().parent().parent().find(".typeAnswer").empty();
        $(this).parent().parent().parent().parent().find(".typeAnswer").append('<div><input type="text" name="" class="descriptionShortAnswer" placeholder="Chọn thành phố" readonly="true"><span class="glyphicon glyphicon-chevron-down"></span></div><div><input type="text" name="" class="descriptionShortAnswer" placeholder="Chọn quận/huyện" readonly="true"><span class="glyphicon glyphicon-chevron-down"></span></div><div><input type="text" name="" class="descriptionShortAnswer" placeholder="Chọn xã/phường" readonly="true"><span class="glyphicon glyphicon-chevron-down"></span></div>');
        $(this).parent().parent().find(".typeAnswerCurrent").text('Địa điểm');
        $(this).parent().parent().parent().parent().find(".typeQuestionId").val('11');
    });

    $('body').on('click', '.getOptionIndex', function (ev) {
        let getId = $(this).attr('id');
        let typeQ = $(this).parent().parent().parent().parent().parent().parent().find('.typeQuestionId').val();
        if (typeQ == 4) {
            $('#' + getId).bind("paste", function (e) {
                // access the clipboard using the api
                var pastedData = e.originalEvent.clipboardData.getData('text');
                var rows = pastedData.split("\n");
                if (rows.length > 1) {
                    e.preventDefault();
                    var popped = rows.pop();
                    $(this).removeClass('countSecondNumberDropMenu');
                    $(this).val(rows[rows.length - 1].split("\t"));
                    for (let i = 0; i < rows.length - 1; i++) {
                        var cells = rows[i].split("\t");
                        $(this).parent().parent().before('<div><label class="labelDropdown"><span class="countFirstNumberDropMenu"></span><input type="text" name="" class="descriptionShortAnswer getOptionIndex" value="' + cells + '" onclick="this.select();"> <a class="btnRemove"><img src="/resources/img/form/closeForm.png" alt=""></a></label></div>');
                    }
                    $(this).parent().parent().parent().find(".countFirstNumberDropMenu").each(function (i) {
                        $(this).text(" " + (i + 1) + ". ");
                    });
                    $('body').find('.getOptionIndex').each(function (i) {
                        $(this).attr('id', 'getOptionIndex' + i);
                    });
                    e.stop();
                }
            });
        } else {
            ev.preventDefault();
        }
    });

    $('body').on('click', '.btnRemoveImg', function (ev) {
        var result = confirm("Bạn có chắc muốn xóa ảnh bìa này không?");
        if (result) {
            $(this).parent().find('#img').removeAttr('src', '');
            let check = $(this).parent().find('#img').attr('src');
            if (!check || check == "null") {
                $(this).parent().css('display', 'none');
            }
        }
    });
    checkDuplicate();
});


function checkDuplicate() {
    var checkDupli = [];
    $('body').on('change', '.rowS', function () {
        let text = $(this).attr('value');
        $(this).parent().parent().parent().find('.rowS').each(function (i) {
            checkDupli[i] = $(this).val();
        });
        for (let i = 0; i < checkDupli.length - 1; i++) {
            for (let j = i + 1; j < checkDupli.length; j++) {
                if (checkDupli[i] == checkDupli[j]) {
                    alert("Đã có hàng tương tự, xin hãy nhập lại!");
                    $(this).val(text);
                    $(this).select();
                }
            }
        }
        if (!($(this).val())) {
            alert('Hãy điền đầy đủ các tùy chọn trong câu hỏi!');
            $(this).val(text);
            $(this).select();
        }
    });

    $('body').on('change', '.colS', function () {
        let text = $(this).attr('value');
        $(this).parent().parent().parent().find('.colS').each(function (i) {
            checkDupli[i] = $(this).val();
        });
        for (let i = 0; i < checkDupli.length - 1; i++) {
            for (let j = i + 1; j < checkDupli.length; j++) {
                if (checkDupli[i] == checkDupli[j]) {
                    alert("Đã có cột tương tự, xin hãy nhập lại!");
                    $(this).val(text);
                    $(this).select();
                }
            }
        }
        if (!($(this).val())) {
            alert('Hãy điền đầy đủ các tùy chọn trong câu hỏi!');
            $(this).val(text);
            $(this).select();
        }
    });
}