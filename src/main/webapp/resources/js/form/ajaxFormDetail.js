var idForm = $("#idForm").val();

//Call api get form
$.ajax({
    url: "/formAPI/forms/api/" + idForm,
    dataType: 'json',
    contentType: "application/json",
    type: 'GET',
}).done(function (result) {
    //Form does not exist, redirect to page error 404
    if (!result) {
        window.location.href = "/public/errorPage/404";
    }

    //Check status form
    if (!result.status) {
        $('body').css('display', 'none');
        window.location.href = "/public/errorPage/notReadyUse";
    }
    $('.viewTitleForm').text(result.title);
    $('.viewDescriptionForm').text(result.shortD);
    $('#img').attr('src', result.image);
});

//Call api get part by form
$.ajax({
    url: "/formAPI/forms/part/" + idForm,
    dataType: 'json',
    contentType: "application/json",
    type: 'GET',
    async: false,

}).done(function (result) {
    result.forEach((item, indexPart) => {
        let a = `<div class="formSection" id="part` + indexPart + `">
                        <input type="hidden" class="numberIndexPart" value="` + indexPart + `">
                        <input type="hidden" class="partFormId" id="partForm` + item.id + `" value="` + item.id + `">
                        <div class="sectionNumber">
                            <label>Phần <span class="countNumberSection"></span></label>
                        </div>
                        <label class="viewtitleFormSection">` + item.title + `</label>
                        <label class="viewdescriptionFormSection">` + item.shortD + `</label>
                    </div>`;
        $(".form-content").append(a);

        //set number part
        $(".countNumberSection").each(function (i) {
            $(this).text(" " + (i + 1));
        });
    })

    //call api get question by each part
    $(".formSection").each(function (i) {
        var inputIdPart = $(this).find('.partFormId').val();
        var partFormId = (inputIdPart == undefined) ? -1 : parseInt(inputIdPart);

        $.ajax({
            url: "/formAPI/forms/part/questions/" + partFormId,
            dataType: 'json',
            contentType: "application/json",
            type: 'GET',
            async: false,

        }).done(function (result) {
            if (result) {
                result.forEach(item => {
                    let a = `<div class="container ctnQuestion">
                                <div class="contentQuestion">
                                    <div class="eachQuestion">
                                        <div class="col-md-12">
                                            <label class="viewEachQuestion">` + item.question + ` <span class="required">*</span></label>
                                            <div>                                             
                                                <img class="imageQuestion" src="` + item.image + `" alt="">
                                            </div>
                                            <div class="typeAnswer" id="` + item.idTypeQuestion + `">
                                                <input type="hidden" id="idQuestion" class="idQuestion` + item.id + `" value="` + item.id + `">
                                                <input type="hidden" id="typeOfAnswer" value="` + item.idTypeQuestion + `">
                                                <input type="hidden" class="requiredShow" value="` + item.required + `">
                                                <input type="hidden" id="idAnswer" value="0">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            `;
                    $("#partForm" + item.idPart).parent().append(a);

                });
            }

            //append content question by type question
            $("#partForm" + partFormId).parent().find('.typeAnswer').each(function (i) {
                let idQuestion = $(this).find('#idQuestion').val();
                let type;

                //type short text answer
                if (this.id == 1) {
                    type = `<textarea name="" rows="3" class="viewDescriptionShortAnswer resultText"
                                       placeholder="Câu trả lời của bạn"></textarea>
                            `;
                    // type multiple choice
                } else if (this.id == 2) {
                    type = $(`<div class="answerRadio"></div>`);
                    $.ajax({
                        url: "/formAPI/forms/part/questions/options/" + idQuestion,
                        dataType: 'json',
                        contentType: "application/json",
                        type: 'GET',
                    }).done(function (result) {

                        result.forEach(item => {
                            let temp = `<div class="radio viewRadio">
                                            <input type="radio" name="optradio` + idQuestion + `" class="checkResult" id="` + item.id + `" value="` + item.optionQuestion + `">
                                            <label>
                                                ` + item.optionQuestion + `
                                            </label>
                                        </div>
                                        `;
                            type.append(temp);
                        });
                    });
                    //type drop-down list
                } else if (this.id == 4) {
                    type = $(`<div id="answerDropdown">
                                 <label class="labelDropdown">
                                     <select class="form-control viewDropdownMenu"
                                          id="exampleFormControlSelect1">
                                          <option value="-1">--Chọn--</option>
                                     </select>                                          
                                 </label>
                             </div>`);
                    $.ajax({
                        url: "/formAPI/forms/part/questions/options/" + idQuestion,
                        dataType: 'json',
                        contentType: "application/json",
                        type: 'GET',
                    }).done(function (result) {

                        result.forEach(item => {
                            let temp = `<option value="` + item.optionQuestion + `">` + item.optionQuestion + `</option>
                                        `;
                            type.find('.viewDropdownMenu').append(temp);
                        });
                    });

                    //type checkboxes
                } else if (this.id == 5) {
                    type = $(`<div class="answerCheckbox"></div>`);
                    $.ajax({
                        url: "/formAPI/forms/part/questions/options/" + idQuestion,
                        dataType: 'json',
                        contentType: "application/json",
                        type: 'GET',
                    }).done(function (result) {

                        result.forEach(item => {
                            let temp = `<div class="checkbox viewCheckbox">
                                            <input type="checkbox" name="optradio" class="checkResult" id="` + item.id + `" value="` + item.optionQuestion + `">
                                            <label>
                                                ` + item.optionQuestion + `
                                            </label>
                                        </div>
                                        `;
                            type.append(temp);
                        });
                    });
                    //type tick box grid
                } else if (this.id == 6) {
                    type = $(`<table class="table"></table>`);
                    let cols = $(`<tr class="tbheader"></tr>`).append(`<td></td>`);
                    var colsLength;
                    $.ajax({
                        url: "/formAPI/forms/questions/optionsGrid/cols/" + idQuestion,
                        dataType: 'json',
                        contentType: "application/json",
                        type: 'GET',
                    }).done(function (result) {
                        colsLength = result.length;
                        // let colsArray = result;
                        result.forEach((item, i) => {
                            let temp = `<td class="cols` + i + `">` + item.cols + `</td>`;
                            cols.append(temp);
                        });
                        type.append(cols);

                        $.ajax({
                            url: "/formAPI/forms/questions/optionsGrid/rows/" + idQuestion,
                            dataType: 'json',
                            contentType: "application/json",
                            type: 'GET',
                        }).done(function (result) {
                            result.forEach((item, index) => {
                                let rows = $(`<tr>
                                               <td class="rowsValue">` + item.rows + `</td>
                                          </tr>`);
                                for (let i = 0; i < colsLength; i++) {
                                    rows.append(`<td>
                                                    <input hidden class="getIndexRow" value="` + i + `">
                                                    <input type="checkbox" class="checkboxRadioGrid resultCheckboxGrid" name="optcheckbox" value="` + item.rows + `">
                                                </td>                                                
                                                `);
                                }
                                type.append(rows);
                            });
                        });
                    });
                    //type multiple-choice grid
                } else if (this.id == 8) {
                    type = $(`<table class="table"></table>`);
                    let cols = $(`<tr class="tbheader"></tr>`).append(`<td></td>`);
                    var colsLength;
                    $.ajax({
                        url: "/formAPI/forms/questions/optionsGrid/cols/" + idQuestion,
                        dataType: 'json',
                        contentType: "application/json",
                        type: 'GET',
                    }).done(function (result) {
                        colsLength = result.length;
                        // let colsArray = result;
                        result.forEach((item, i) => {
                            let temp = `<td class="cols` + i + `">` + item.cols + `</td>`;
                            cols.append(temp);
                        });
                        type.append(cols);

                        $.ajax({
                            url: "/formAPI/forms/questions/optionsGrid/rows/" + idQuestion,
                            dataType: 'json',
                            contentType: "application/json",
                            type: 'GET',
                        }).done(function (rowS) {
                            rowS.forEach(item => {
                                let rows = $(`<tr>
                                               <td class="rowsValue">` + item.rows + `</td>
                                          </tr>`);
                                for (let i = 0; i < colsLength; i++) {
                                    rows.append(`<td>
                                                    <input hidden class="getIndexRow" value="` + i + `">
                                                    <input type="radio" class="checkboxRadioGrid resultRadioGrid" name="optradio` + item.id + `" value="` + item.rows + `">
                                                </td>                                                
                                                `);
                                }
                                type.append(rows);
                            });
                        });
                    });
                    //type date
                } else if (this.id == 9) {
                    type = ` <input type="date" class="form-control dateCalenda descriptionShortAnswer viewDate datepicker resultText hasDatepicker" placeholder="Ngày, tháng, năm">
                                    `;
                    //type time
                } else if (this.id == 10) {
                    type = `<div class="containInputTime">
                                <input type="text" class="form-control timepicker descriptionShortAnswer viewTime" />
                                <span class="glyphicon glyphicon-time"></span></p>   
                            </div>
           
                                    `;
                    //type local, address, city, district, communt
                } else if (this.id == 11) {
                    type = `<div class="col-md-4 viewAddress">
                                        <select class="form-control city" id="city1">
                                            <option value="-1">Chọn thành phố</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4 viewAddress">
                                        <select class="form-control district" id="district1" disabled>
                                            <option value="-1">Chọn quận/huyện</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4 viewAddress">
                                        <select class="form-control commune" id="commune1" disabled>
                                            <option value="-1">Chọn xã phường</option>
                                        </select>
                                    </div>
                                    `;
                }
                $(this).append(type);
            });

            //call api get all city in VietNam
            $.ajax({
                url: "/formAPI/address/city",
                dataType: 'json',
                contentType: "application/json",
                type: 'GET',
            }).done(function (result) {
                result.forEach(item => {
                    let a = $("<option></option>", {
                        html: item.city,
                        value: item.idCity,
                    })
                    $(".city").append(a);
                })
            });

            //event when change select city, get district by city
            $('.city').on('change', function (j) {
                $(this).parent().parent().find('.district').empty();
                $(this).parent().parent().find('.district').append('<option value="-1" >Chọn quận/huyện</option>');
                $(this).parent().parent().find('.district').removeAttr('disabled');
                $(this).parent().parent().find('.commune').empty();
                $(this).parent().parent().find('.commune').append('<option value="-1" >Chọn xã phường</option>');

                $.ajax({
                    url: "/formAPI/address/district/" + this.value,
                    dataType: 'json',
                    contentType: "application/json",
                    type: 'GET',

                    success: function (result) {
                        result.forEach((item, i) => {
                            let a = $("<option></option>", {
                                html: item.districtName,
                                value: item.idDistrict,
                            })
                            $('.district').append(a);
                        });
                    }
                })
                $(this).stopPropagation();
            });

            //event when change select district, get commune by district
            $('.district').on('change', function () {
                $(this).parent().parent().find('.commune').empty();
                $(this).parent().parent().find('.commune').append('<option value="-1" >Chọn xã phường</option>');
                $(this).parent().parent().find('.commune').removeAttr('disabled');
                $.ajax({
                    url: "/formAPI/address/commune/" + this.value,
                    dataType: 'json',
                    contentType: "application/json",
                    type: 'GET',
                }).done(function (result) {
                    result.forEach(item => {
                        let a = $("<option></option>", {
                            html: item.communeName,
                            value: item.idPx,
                        })
                        $(".commune").append(a);
                    })
                });
                $(this).stopPropagation();
            });

            //call api get Answer id for each question
            let idForm = $('#idForm').val();
            let numberPart = $('.formSection').length;
            let idCurrent = [];
            $.ajax({
                url: "/formAPI/report/answers/" + idForm,
                dataType: 'json',
                contentType: "application/json",
                type: 'GET',
                async: false,

                success: function (answers) {
                    if (answers) {
                        answers.forEach((item, i) => {
                            let idAnswer = item.id;
                            $('.idQuestion' + item.idquestion).parent().find('#idAnswer').val(idAnswer);
                        });
                    }
                },
            });

            //get all ID of question grid
            $('.eachQuestion').each(function (index) {
                let typeQuestion = $(this).find('#typeOfAnswer').val();
                let idAnswer = $(this).find('#idAnswer').val();
                if ((typeQuestion == 6 || typeQuestion == 8) && i == (numberPart - 1)) {
                    idCurrent.push(idAnswer);
                }
            });


            let check = false;
            if (i == (numberPart - 1)) {
                idCurrent.forEach((item, i) => {
                    $.ajax({
                        url: "/formAPI/report/answer/getIdByForm/" + idForm,
                        dataType: 'json',
                        contentType: "application/json",
                        type: 'GET',
                        async: false,

                        success: function (listId) {
                            for (let j = 0; j < listId.length; j++) {
                                if ((i == j) && (item == listId[j])) {
                                    check = true;
                                }
                            }
                        },
                    });
                });

                if (!check) {
                    $('.eachQuestion').each(function (indexQ) {

                        let idQ = $(this).find('#idQuestion').val();
                        let idAnswer = $(this).find('#idAnswer').val();
                        let typeQuestion = $(this).find('#typeOfAnswer').val();
                        if ((typeQuestion == 6 || typeQuestion == 8) && i == (numberPart - 1)) {

                            $.ajax({
                                url: "/formAPI/forms/questions/optionsGrid/rows/" + idQ,
                                dataType: 'json',
                                contentType: "application/json",
                                type: 'GET',
                                async: false,

                                success: function (rows) {
                                    rows.forEach((r, i) => {
                                        $.ajax({
                                            url: "/formAPI/forms/questions/optionsGrid/cols/" + idQ,
                                            dataType: 'json',
                                            contentType: "application/json",
                                            type: 'GET',
                                            async: false,
                                            success: function (cols) {
                                                cols.forEach((c, i) => {
                                                    dataColChart = {};
                                                    dataColChart["row"] = r.rows;
                                                    dataColChart["col"] = c.cols;
                                                    dataColChart["count"] = 0;
                                                    dataColChart["idAnswer"] = idAnswer;

                                                    $.ajax({
                                                        url: '/formAPI/report/answerColChart',
                                                        data: JSON.stringify(dataColChart),
                                                        type: "POST",
                                                        contentType: "application/json; charset=utf-8",
                                                        dataType: "json",
                                                        async: false,
                                                    });
                                                });
                                            },
                                        });
                                    });
                                },
                            });
                        }
                    });
                }

                //check question required
                $('.eachQuestion').each(function () {
                    let required = $(this).find('.requiredShow').val();
                    if (required == "false") {
                        $(this).find('.required').css("display", "none");
                    }
                });

                //check src image null
                $(".imageQuestion").each(function (i) {
                    let check = $(this).attr('src');
                    if (!check || check == "null") {
                        $(this).parent().css('display', 'none');
                    }
                });

            }
        });
    });

    //paging form by part, first page show button 'next', last page show button 'previous','submit'
    var numberPart = $('.partFormId').length;
    if (numberPart > 1) {
        $('.btnPrevious').css('display', 'none');
        $('.btnSubmit').css('display', 'none');
    } else {
        $('.btnPrevious').css('display', 'none');
        $('.btnNext').css('display', 'none');
        $('.progressPart').css('display', 'none');
    }

    //display part 2 onwards when load page
    for (let i = 1; i < numberPart; i++) {
        $('#part' + i).css('display', 'none');
    }

    //set part 1st to part selected
    $('#part0').addClass('partSelected');

    //progress
    $('.numberAllPart').text(numberPart);
    $('#progressPart').attr('max', numberPart);
});


