$('body').on('click', '.btnSubmit', function () {
    $(this).prop('disabled', true);

    let checkRe = true;

    $('.eachQuestion').each(function () {
        $(this).parent().parent().css("border-color", "#144E8C");
        $(this).parent().parent().css("box-shadow", "none");
        let textContent;
        let required = $(this).find('.requiredShow').val();
        let typeQ = $(this).find('#typeOfAnswer').val();
        let resultPie = [];
        let resultRow = [];
        let resultCol = [];

        if (required == "true") {
            if (typeQ == 1 || typeQ == 9) {
                textContent = $(this).find('.resultText').val();
                if (!textContent) {
                    checkRe = false;

                    $(this).parent().parent().css("border-color", "red");
                    $(this).parent().parent().css("box-shadow", "-5px 0px 0px rgba(255, 0, 0, 1)");
                }
            }
            if (typeQ == 10) {
                let time = $(this).find('.viewTime').val();


                if (!time) {
                    checkRe = false;

                    $(this).parent().parent().css("border-color", "red");
                    $(this).parent().parent().css("box-shadow", "-5px 0px 0px rgba(255, 0, 0, 1)");
                }
            }
            if (typeQ == 11) {
                let city = $(this).find('#city1 :selected').val();
                let district = $(this).find('#district1 :selected').val();
                let commune = $(this).find('#commune1 :selected').val();

                if (city == -1 || district == -1 || commune == -1) {
                    checkRe = false;

                    $(this).parent().parent().css("border-color", "red");
                    $(this).parent().parent().css("box-shadow", "-5px 0px 0px rgba(255, 0, 0, 1)");
                }
            }
            if (typeQ == 2) {
                resultPie[0] = $(this).find('.checkResult:checked').val();

                if (!resultPie[0]) {
                    checkRe = false;

                    $(this).parent().parent().css("border-color", "red");
                    $(this).parent().parent().css("box-shadow", "-5px 0px 0px rgba(255, 0, 0, 1)");
                }
            }
            if (typeQ == 4) {
                resultPie[0] = $(this).find('.viewDropdownMenu :selected').val();
                if (!resultPie[0] || resultPie[0] == -1) {
                    checkRe = false;

                    $(this).parent().parent().css("border-color", "red");
                    $(this).parent().parent().css("box-shadow", "-5px 0px 0px rgba(255, 0, 0, 1)");
                }
            }
            if (typeQ == 5) {
                var val = [];
                $(this).find(':checkbox:checked').each(function (i) {
                    val[i] = $(this).val();
                });
                resultPie = val;

                if (resultPie.length == 0) {
                    checkRe = false;

                    $(this).parent().parent().css("border-color", "red");
                    $(this).parent().parent().css("box-shadow", "-5px 0px 0px rgba(255, 0, 0, 1)");
                }
            }
            if (typeQ == 6) {
                let rows = [];
                let cols = [];
                $(this).find('.resultCheckboxGrid:checked').each(function (i) {
                    rows[i] = $(this).val();

                    let indexRow = $(this).parent().find('.getIndexRow').val();

                    cols[i] = $(this).parent().parent().parent().find('.cols' + indexRow).text();


                });
                if (rows.length > 0) {
                    resultCol = cols;
                    resultRow = rows;
                }
                if (resultRow.length == 0) {
                    checkRe = false;

                    $(this).parent().parent().css("border-color", "red");
                    $(this).parent().parent().css("box-shadow", "-5px 0px 0px rgba(255, 0, 0, 1)");
                }
            }
            if (typeQ == 8) {
                let rowss = [];
                let colss = [];
                $(this).find('.resultRadioGrid:checked').each(function (i) {
                    rowss[i] = $(this).val();

                    let indexRow = $(this).parent().find('.getIndexRow').val();

                    colss[i] = $(this).parent().parent().parent().find('.cols' + indexRow).text();


                });
                if (rowss.length > 0) {
                    resultCol = colss;
                    resultRow = rowss;
                }
                if (resultRow.length == 0) {
                    checkRe = false;

                    $(this).parent().parent().css("border-color", "red");
                    $(this).parent().parent().css("box-shadow", "-5px 0px 0px rgba(255, 0, 0, 1)");
                }
            }
        }
    });

    if (!checkRe) {
        alert("Xin hãy điền tất cả câu hỏi bắt buộc!");
        $('.btnSubmit').removeProp('disabled');
    } else {

        $('body').css("pointer-events", "none");
        $('body').css("opacity", "0.4");
        $('body').append('<div class="spinner"></div>');

        $('.eachQuestion').each(function () {
            let typeQ = $(this).find('#typeOfAnswer').val();
            let idQ = $(this).find('#idQuestion').val();
            let required = $(this).find('.requiredShow').val();
            let resultText;

            let resultPie = [];

            let resultRow = [];
            let resultCol = [];

            if (typeQ == 1 || typeQ == 9) {
                resultText = $(this).find('.resultText').val();

                resultText = resultText ? resultText : -1;
            }
            if (typeQ == 10) {
                let time = $(this).find('.viewTime').val();

                resultText = (time) ? time : -1;
            }
            if (typeQ == 11) {
                city = $(this).find('.city :selected').val();
                district = $(this).find('.district :selected').val();
                commune = $(this).find('.commune :selected').val();

                cityText = city != -1 ? $(this).find('.city :selected').text() : -1;
                districtText = district != -1 ? $(this).find('.district :selected').text() : -1;
                communeText = commune != -1 ? $(this).find('.commune :selected').text() : -1;

                if (cityText != -1) {
                    resultText = cityText;
                }
                if (districtText != -1) {
                    resultText = resultText + ', ' + districtText;
                }
                if (communeText != -1) {
                    resultText = resultText + ', ' + communeText;
                }
                if (city == -1 && district == -1 && commune == -1) {
                    resultText = -1;
                }
            }
            if (typeQ == 2) {
                resultPie[0] = $(this).find('.checkResult:checked').val();
            }
            if (typeQ == 4) {
                resultPie[0] = $(this).find('.viewDropdownMenu :selected').val();
            }
            if (typeQ == 5) {
                var val = [];
                $(this).find(':checkbox:checked').each(function (i) {
                    val[i] = $(this).val();
                });
                resultPie = val;
            }
            if (typeQ == 6) {
                let rows = [];
                let cols = [];
                $(this).find('.resultCheckboxGrid:checked').each(function (i) {
                    rows[i] = $(this).val();

                    let indexRow = $(this).parent().find('.getIndexRow').val();

                    cols[i] = $(this).parent().parent().parent().find('.cols' + indexRow).text();


                });
                if (rows.length > 0) {
                    resultCol = cols;
                    resultRow = rows;
                }
            }
            if (typeQ == 8) {
                let rowss = [];
                let colss = [];
                $(this).find('.resultRadioGrid:checked').each(function (i) {
                    rowss[i] = $(this).val();

                    let indexRow = $(this).parent().find('.getIndexRow').val();

                    colss[i] = $(this).parent().parent().parent().find('.cols' + indexRow).text();


                });
                if (rowss.length > 0) {
                    resultCol = colss;
                    resultRow = rowss;
                }
            }

            $.ajax({
                url: "/formAPI/report/answers/get/" + idQ,
                dataType: 'json',
                contentType: "application/json",
                type: 'GET',
                async: false,

                success: function (result) {
                    if (result.idtypeshow == 1) {
                        dataAnswerUser = {};
                        dataAnswerUser["answer"] = resultText;
                        dataAnswerUser["idanswer"] = result.id;
                        if (resultText != -1) {
                            $.ajax({
                                url: '/formAPI/report/answertext',
                                data: JSON.stringify(dataAnswerUser),
                                type: "POST",
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                async: false,
                            });
                        }
                    }
                    if (result.idtypeshow == 2) {
                        dataAnswerUser = {};
                        dataAnswerUser["idanswer"] = result.id;

                        if (resultPie.length > 1) {
                            for (let i = 0; i < resultPie.length; i++) {
                                dataAnswerUser["answer"] = resultPie[i];

                                $.ajax({
                                    url: '/formAPI/report/answerpiechart',
                                    data: JSON.stringify(dataAnswerUser),
                                    type: "POST",
                                    contentType: "application/json; charset=utf-8",
                                    dataType: "json",
                                    async: false,
                                });

                            }
                        } else {
                            dataAnswerUser["answer"] = resultPie[0];

                            if (resultPie[0] != -1) {
                                $.ajax({
                                    url: '/formAPI/report/answerpiechart',
                                    data: JSON.stringify(dataAnswerUser),
                                    type: "POST",
                                    contentType: "application/json; charset=utf-8",
                                    dataType: "json",
                                    async: false,
                                });
                            }
                        }
                    }
                    if (result.idtypeshow == 3) {
                        dataAnswerUser = {};
                        dataAnswerUser["idanswer"] = result.id;

                        for (let i = 0; i < resultRow.length; i++) {
                            dataAnswerUser["row"] = "" + resultRow[i];
                            dataAnswerUser["col"] = "" + resultCol[i];

                            $.ajax({
                                url: "/formAPI/report/answer/setCount/" + dataAnswerUser.row + "/" + dataAnswerUser.col + "/" + dataAnswerUser.idanswer,
                                dataType: 'json',
                                contentType: "application/json; charset=utf-8",
                                type: 'PUT',
                                async: false,
                            });
                        }
                    }
                }
                ,
            });
        });
        var idForm = $("#idForm").val();
        $.ajax({
            url: "/formAPI/form/countanswer/" + idForm,
            dataType: 'json',
            contentType: "application/json",
            type: 'PUT',
            success(numberPage) {
            }
        })
        window.location.href = "/public/forms/complete/" + idForm;
    }
});

