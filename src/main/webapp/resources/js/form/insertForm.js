var getFormID = ($('#idForm').val()) ? $('#idForm').val() : -1;
if (getFormID == -1) {
    $('.previewForm').on('click', function () {
        alert("Lưu trước khi muốn xem trước biểu mẫu!")
    });
    $('body').on('click', '.btn-result', function (e) {
        e.preventDefault();
    });
    $('body').on('click', '.btn-question', function (e) {
        e.preventDefault();
    });
}
$('.create').on('click', function (e) {
    let countCheckNullQuestion = 0;
    let contentIndexQuestion = [];
    let countCheckOption = 0;
    let contentIndexOption = [];
    let a = $(this).attr('id');
    let copyValue = $('.inputTitleCopy').val()
    dataForm = {};
    if (a == 'copyForm') {
        dataForm["title"] = copyValue;

    } else {
        dataForm["title"] = $('#title').val();
    }

    dataForm["shortD"] = $('#description').val();
    dataForm["image"] = $('#img').attr('src');

    $('.titleShortAnswer').each(function (i) {
        if (!($(this).val())) {
            countCheckNullQuestion++;
            contentIndexQuestion.push($(this).attr('id'));
        }
    });

    $('.labelDropdown').find('.descriptionShortAnswer').each(function (i) {
        if (!($(this).val())) {
            countCheckOption++;
            contentIndexOption.push($(this).attr('id'));
        }
    });

    if (!dataForm.title) {
        if (a != 'copyForm') {
            alert('Bạn chưa điền "Tiêu đề khảo sát"!');
            window.location.href = "#title";
            setTimeout(function () {
                $(window).scrollTop($(window).scrollTop() - 100);
            }, 10);
        }
    } else if (countCheckNullQuestion > 0) {
        alert('Hãy điền đầy đủ các câu hỏi!');
        window.location.href = "#" + contentIndexQuestion[0];
        setTimeout(function () {
            $(window).scrollTop($(window).scrollTop() - 100);
        }, 10);
    } else if (countCheckOption > 0) {
        alert('Hãy điền đầy đủ các tùy chọn trong câu hỏi!');
        window.location.href = "#" + contentIndexOption[0];
        setTimeout(function () {
            $(window).scrollTop($(window).scrollTop() - 100);
        }, 10);
    } else {
        $('body').css("pointer-events", "none");
        $('body').css("opacity", "0.4");
        $('body').append('<div class="spinner"></div>');

        // console.log(dataForm.title + ' - ' + dataForm.shortD);
        async function insertFormAwait() {
            await $.ajax({
                headers: {"Authorization": 'Bearer ' + localStorage.getItem("eln_token")},
                url: '/formAPI/forms/insert',
                data: JSON.stringify(dataForm),
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,

                success: function (formResult) {
                    var idForm = formResult.id;

                    if (getFormID == -1) {
                        alert("Tạo mới biểu mẫu thành công!");
                    }

                    $('.formSection').each(function (indexSection) {
                        dataPart = {};
                        dataPart["title"] = $('#titleFormSection' + indexSection).val();
                        dataPart["shortD"] = $('#descriptionFormSection' + indexSection).val();
                        dataPart["idForm"] = idForm;

                        // console.log(dataPart.title + ' - ' + dataPart.shortD + ' - ' + dataPart.idForm);

                        insertPartAwait(indexSection, idForm);
                    });
                },
                error: function (formResult) {
                    alert("Thêm thất bại");
                },
            });
        }

        async function insertPartAwait(indexSection, idForm) {
            await $.ajax({
                url: '/formAPI/forms/part/insert',
                data: JSON.stringify(dataPart),
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,

                success: function (dataPart) {
                    var idPart = dataPart.id;

                    $(this).find('.getIdPart').val(idPart);
                    $(this).find('.getIdPart').attr('id', 'getIdPart' + idPart);

                    $('#sectionId' + indexSection).find('.ctnQuestion').each(function (indexQuestion) {
                        dataQuestion = {};
                        dataQuestion["question"] = $(this).find('.titleShortAnswer').val();
                        dataQuestion["idTypeQuestion"] = $(this).find('.typeQuestionId').val();
                        dataQuestion["required"] = "" + $(this).find('.myCheck').is(":checked");
                        dataQuestion["idPart"] = idPart;
                        dataQuestion["image"] = $(this).find('.imageQuestion').attr('src');

                        // console.log(dataQuestion.question + ' - ' + dataQuestion.idTypeQuestion + ' - ' + dataQuestion.required + ' - ' + dataQuestion.idPart);

                        insertQuestionAwait(indexSection, indexQuestion, idForm);
                    });
                    if (getFormID == -1) {
                        window.location.href = "/home";
                    } else {
                        window.location.href = "/forms/edit/" + idForm;
                    }
                },
            });
        }

        async function insertQuestionAwait(indexSection, indexQuestion, idForm) {
            await $.ajax({
                url: '/formAPI/forms/question/insert',
                data: JSON.stringify(dataQuestion),
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,

                success: function (dataQuestion) {
                    var idQuestion = dataQuestion.id;
                    var idTypeQs = dataQuestion.idTypeQuestion;
                    let idAnswer;
                    dataAnswer = {};
                    dataAnswer["idquestion"] = idQuestion;
                    dataAnswer["idform"] = idForm;
                    if (idTypeQs == 2 || idTypeQs == 4 || idTypeQs == 5) {
                        dataAnswer["idtypeshow"] = 2;
                    } else if (idTypeQs == 6 || idTypeQs == 8) {
                        dataAnswer["idtypeshow"] = 3;
                    } else {
                        dataAnswer["idtypeshow"] = 1;
                    }
                    // alert(dataAnswer.idquestion + ' - ' + dataAnswer.idform + ' - ' + dataAnswer.idtypeshow);
                    insertAnswerReportAwait();

                    $('#getIdQuestion' + indexSection + indexQuestion).val(idQuestion);

                    // console.log(indexSection + '' + indexQuestion + ' - ' + idQuestion);

                    if (idTypeQs == 2 || idTypeQs == 4 || idTypeQs == 5) {
                        $('#getIdQuestion' + indexSection + indexQuestion).parent().find('.getOptionIndex').each(function (i) {
                            // console.log($(this).val() + ' - ' + idQuestion);
                            dataOptions = {};
                            dataOptions["optionQuestion"] = $(this).val();
                            dataOptions["idQuestion"] = idQuestion;

                            insertOptionQuestionAwait();
                        });
                    }

                    if (idTypeQs == 6 || idTypeQs == 8) {
                        sizeOptionRows = $('#getIdQuestion' + indexSection + indexQuestion).parent().find('.getOptionRowsIndex').length;
                        sizeOptionCols = $('#getIdQuestion' + indexSection + indexQuestion).parent().find('.getOptionColsIndex').length;

                        temp = sizeOptionRows >= sizeOptionCols ? $('#getIdQuestion' + indexSection + indexQuestion).parent().find('.getOptionRowsIndex')
                            : $('#getIdQuestion' + indexSection + indexQuestion).parent().find('.getOptionColsIndex');

                        temp.each(function (i) {
                            idAnswer = $(this).parent().parent().parent().parent().parent().parent().parent().parent().parent().find('.idAnswer').val();
                            textRows = $(this).parent().parent().parent().parent().parent().find('#indexOptionRows' + i).val();
                            textCols = $(this).parent().parent().parent().parent().parent().find('#indexOptionCols' + i).val();

                            textRows = textRows == undefined ? '-1' : textRows;
                            textCols = textCols == undefined ? '-1' : textCols;
                            dataOptionsGrid = {};
                            dataOptionsGrid["rows"] = textRows;
                            dataOptionsGrid["cols"] = textCols;
                            dataOptionsGrid["idQuestion"] = idQuestion;

                            // console.log(dataOptionsGrid.rows + ' - ' + dataOptionsGrid.cols + ' - ' + dataOptionsGrid.idQuestion);

                            insertOptionGridQuestionAwait();
                        });
                    }
                },
            });
        }

        async function insertOptionQuestionAwait() {
            await $.ajax({
                url: '/formAPI/forms/part/questions/options/insert',
                data: JSON.stringify(dataOptions),
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
            });
        }


        async function insertOptionGridQuestionAwait() {
            await $.ajax({
                url: '/formAPI/forms/part/questions/optionsGrid/insert',
                data: JSON.stringify(dataOptionsGrid),
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
            });
        }


        async function insertAnswerReportAwait() {
            await $.ajax({
                url: '/formAPI/report/answers',
                data: JSON.stringify(dataAnswer),
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,

                success: function (dataAnswer) {
                    $('.eachQuestion').each(function (i) {
                        $(this).find('.idAnswer').val(dataAnswer.id);
                    });
                }
            });
        }


        insertFormAwait();
    }
});
