var getIdForm = ($('#idForm').val()) ? $('#idForm').val() : -1;
if (getIdForm != -1) {
    $('.previewForm').find('a').attr("href", "/public/forms/detail/" + getIdForm);
    $('.btnSubmit').css('display', 'none');
    $('#create').text('Sửa biểu mẫu');
    $('.titleAndDescription').css('padding-top', '17px');
    $('.copyForm').css('display', 'block');

    selectFormAwait();

    async function selectFormAwait() {
        await $.ajax({
            headers: {"Authorization": 'Bearer ' + localStorage.getItem("eln_token")},
            url: "/formAPI/forms/APIEdit/" + getIdForm,
            dataType: 'json',
            contentType: "application/json",
            type: 'GET',


            success: function (forms) {
                if (!forms) {
                    alert("Biểu mẫu không tồn tại!");
                    window.location.href = "/home";
                }
                if (forms.countAnswers == 0) {
                    $('.btn-save').css('display', 'block');
                } else {
                    $('.eventAddQuestion').unbind();
                    $('.eventAddSection').unbind();
                    $('.glyphicon-remove-circle').css('display', 'none');
                    $('.imgCover').children().removeAttr('data-target');
                }
                $('#title').val(forms.title);
                $('#description').val(forms.shortD);
                $('#img').attr('src', forms.image);
                $('#countAnswers').val(forms.countAnswers);
                let temp = forms.title.split(" ");
                let titleShortDes;
                if (temp.length > 4) {
                    titleShortDes = temp[0];
                    for (let i = 1; i < 4; i++) {
                        titleShortDes = titleShortDes + ' ' + temp[i];
                    }

                    titleShortDes = titleShortDes + '...';
                } else {
                    titleShortDes = forms.title;
                }
                $('.homePage').append(`<span class="titleShortDes">❯ ` + titleShortDes + `</span>`);


                var srcCoverImg = $('#img').attr('src');
                if (forms.image) {
                    $('.btnRemoveImg').css('display', 'block');
                }

                let countAnser = $('#countAnswers').val();
                if (countAnser > 0) {
                    $('.contentMessageNew').append(`<div class="alert alert-success" role="alert">
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <strong>Thông báo:</strong> Biểu mẫu đang được sử dụng, không thể lưu thay đổi!
                                            </div>`);
                }

                let checkCountAnswerA = $('#countAnswers').val();
                if (checkCountAnswerA > 0) {
                    $('body').on('click', '.glyphicon-trash', function () {
                        alert("không thể thao tác");
                    });
                    $('body').on('click', '.glyphicon-picture', function () {
                        alert("không thể thao tác");
                    });
                    $('body').on('click', '.alertMessageRemoveImgQ', function () {
                        alert("không thể thao tác");
                    });
                    $('body').on('click', '.alerRemoveSection', function () {
                        alert("không thể thao tác");
                    });
                    $('body').on('click', '.btnTypeQuestion', function () {
                        alert("không thể thao tác");
                    });
                    $('body').on('click', '.imgCover', function () {
                        alert("không thể thao tác");
                    });
                    $('body').on('click', '.eventAddSection', function () {
                        alert("không thể thao tác");
                    });
                    $('body').on('click', '.eventAddQuestion', function () {
                        alert("không thể thao tác");
                    });
                }
            }
        });
        $('.formSection').remove();
        await $.ajax({
            url: "/formAPI/forms/part/" + getIdForm,
            dataType: 'json',
            contentType: "application/json",
            type: 'GET',

            success: function (parts) {
                parts.forEach(item => {
                    let a = `<div class="formSection" id="sectionId0">
                                <input type="hidden" class="indexSection" value="0">                                
                                <input type="hidden" class="getIdPart" id="getIdPart` + item.id + `" value="` + item.id + `">
                                <div class="dropdown drdRemoveSection">
                                    <a class="dropdown-toggle" data-toggle="dropdown">
                                        <span class="glyphicon glyphicon-option-vertical"></span>
                                    </a>
                                    <ul class="dropdown-menu drdMenu">
                                        <li class="btnRemoveSection"><a>Xóa phần</a></li>
                                    </ul>
                                </div>
                                <div class="sectionNumber">
                                    <label>Phần <span class="countNumberSection"></span></label>
                                </div>
                                <input type="text" class="titleFormSection" placeholder="Tiêu đề" id="titleFormSection0" value="` + item.title + `" autocomplete="off">
                                <input type="text" class="descriptionFormSection" placeholder="Thêm mô tả"
                                    id="descriptionFormSection0" value="` + item.shortD + `" autocomplete="off">
                             </div>
                            `;
                    $(".form-content").append(a);

                    let countAnswer = $('#countAnswers').val();
                    $(".countNumberSection").each(function (i) {
                        $(this).text(" " + (i + 1));
                        if (countAnswer > 0) {
                            $(this).parent().parent().parent().find('.btnRemoveSection').addClass('alerRemoveSection');
                            $(this).parent().parent().parent().find('.btnRemoveSection').removeClass('btnRemoveSection');
                        }
                    });
                });

                $('.formSection').each(function (i) {
                    $(this).attr('id', 'sectionId' + i);
                    $(this).find('.titleFormSection').attr('id', 'titleFormSection' + i);
                    $(this).find('.descriptionFormSection').attr('id', 'descriptionFormSection' + i);
                    $(this).find('.indexSection').val(i);

                    let idPart = $(this).find('.getIdPart').val();

                    selectQuestionAwait(idPart, i)
                });
            }
        });
    }

    async function selectQuestionAwait(idPart, i) {
        await $.ajax({
            url: "/formAPI/forms/part/questions/" + idPart,
            dataType: 'json',
            contentType: "application/json",
            type: 'GET',
            async: false,
            success: function (questions) {
                if (questions) {
                    questions.forEach((item, iindex) => {
                        // alert(i + '' + iindex);
                        let a = `<div class="container ctnQuestion">
                                            <div class="contentQuestion">
                                                <a>
                                                    <span class="glyphicon glyphicon-option-vertical optionIconQuestion"></span>
                                                </a>
                                                <div class="eachQuestion">
                                                    <input type="hidden" class="idAnswer" value="0">
                                                    <input type="hidden" class="typeQuestionId" value="` + item.idTypeQuestion + `">
                                                    <input type="hidden" class="getIdQuestion" value="` + item.id + `" id="getIdQuestion` + i + iindex + `">                                                    
                                                    <div class=" flLeft">
                                                        <input type="text" name="" class="titleShortAnswer" placeholder="Đặt câu hỏi" value="` + item.question + `" autocomplete="off">
                                                        <div class="contentImgQuestion">
                                                            <div class="dropdown drdRemoveImage">
                                                                <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                                                    <span class="glyphicon glyphicon-option-vertical"></span>
                                                                </a>
                                                                <ul class="dropdown-menu drdMenu">
                                                                    <li class="btnRemoveQuestion"><a>Xóa ảnh</a></li>
                                                                </ul>
                                                            </div>
                                                            <img class="imageQuestion" src="` + item.image + `" alt="">
                                                        </div>
                                                        <div class="typeAnswer">

                                                        </div>
                                                    </div>
                                                    <div class="flRight">
                                                        <div class="dropdown">
                                                            <button class="btn btn-primary dropdown-toggle btnTypeQuestion" type="button"
                                                                    data-toggle="dropdown">
                                                                <span class="typeAnswerCurrent" id="typeAnswerCurrent` + item.id + `"></span>
                                                                <span class="glyphicon glyphicon-chevron-down"></span>
                                                            </button>
                                                            <ul class="dropdown-menu">
                                                                <li class="shortAnswert"><a>Đoạn câu hỏi</a></li>
                                                                <li class="divider"></li>
                                                                <li class="multipleChoice"><a>Trắc nghiệm</a></li>
                                                                <li class="dropDownAnswer"><a>Menu thả</a></li>
                                                                <li class="checkboxAnswer"><a>Hộp kiểm</a></li>
                                                                <li class="divider"></li>
                                                                <li class="checkboxAnswerGrid"><a>Lưới hộp kiểm</a></li>
                                                                <li class="multipleChoiceGrid"><a>Lưới trắc nghiệm</a></li>
                                                                <li class="divider"></li>
                                                                <li class="dateAnswer"><a>Ngày</a></li>
                                                                <li class="timeAnswer"><a>Giờ</a></li>
                                                                <li class="divider"></li>
                                                                <li class="addressAnswer"><a>Địa chỉ</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="controlQuestion col-md-12">
                                                    <ul>
                                                        <li><input class="myCheck" type="checkbox" value=""> Câu hỏi bắt buộc</li>
                                                        <li>
                                                            <a href="#" data-toggle="modal" data-target="#myModal">
                                                                <span class="glyphicon glyphicon-picture"></span>
                                                            </a>
                                                        </li>
                                                        <li class="removeQuestion">
                                                        <a>
                                                            <span class="glyphicon glyphicon-trash"></span>
                                                        </a>
                                                        </li>
                                                    </ul>
                                                </div>

                                            </div>
                                        </div>
                                        `;
                        $('#getIdPart' + item.idPart).parent().append(a);
                        $('#getIdQuestion' + i + iindex).parent().parent().each(function () {
                            item.required ? $(this).find('.myCheck').attr('checked', true) : '';
                        });
                    });
                }
                let checkCountAnswer = $('#countAnswers').val();
                if (checkCountAnswer > 0) {
                    $('.glyphicon-picture').each(function (i) {
                        $(this).parent().removeAttr('data-target');
                        $(this).parent().parent().parent().find('.removeQuestion').removeClass('removeQuestion');
                        $(this).parent().parent().parent().find('.myCheck').attr('disabled', true);
                        $(this).parent().parent().parent().parent().parent().find('.btnTypeQuestion').removeAttr('data-toggle');
                        $(this).parent().parent().parent().parent().parent().find('.btnRemoveQuestion').addClass('alertMessageRemoveImgQ');
                        $(this).parent().parent().parent().parent().parent().find('.btnRemoveQuestion').removeClass('btnRemoveQuestion');
                    });
                }

                $('.imageQuestion').each(function (i) {
                    $(this).attr('id', 'imageQuestion' + i);
                });

                $(".imageQuestion").each(function (i) {
                    let check = $(this).attr('src');
                    if (!check || check == "null") {
                        $(this).parent().css('display', 'none');
                    }
                });

                $('.titleShortAnswer').each(function (i) {
                    $(this).attr('id', 'titleShortAnswer' + i);
                });

                $('#getIdPart' + idPart).parent().find('.eachQuestion').each(function (i) {
                    let answerContent = $(this).find('.typeAnswer');
                    let idQuestion = $(this).find('.getIdQuestion').val();
                    let idTypeQuesion = $(this).find('.typeQuestionId').val();
                    let type;


                    if (idTypeQuesion == 1) {
                        type = `<input type="text" name="" class="descriptionShortAnswer" placeholder="Đoạn trả lời"
                                       readonly="true" autocomplete="off">
                                            `;
                        $('#typeAnswerCurrent' + idQuestion).text('Đoạn câu hỏi');
                    } else if (idTypeQuesion == 2) {
                        type = $(`<div class="answerMulti"></div>`);

                        $.ajax({
                            url: "/formAPI/forms/part/questions/options/" + idQuestion,
                            dataType: 'json',
                            contentType: "application/json",
                            type: 'GET',
                            async: false,

                            success: function (options) {
                                if (options) {
                                    options.forEach(item => {
                                        let temp = `<div>
                                                                <label class="labelDropdown">
                                                                    <input type="radio" disabled class="radioMulti" name="optradio">
                                                                    <input type="hidden" class="getIdOption" value="` + item.id + `">
                                                                    <input type="text" name="" class="descriptionShortAnswer getOptionIndex" id="getOptionIndex0" value="` + item.optionQuestion + `" onclick="this.select();">
                                                                    <a class="btnRemove"><img src="/resources/img/form/closeForm.png"alt=""></a>
                                                                </label>
                                                            </div>
                                                            `;
                                        type.append(temp);
                                    });
                                }
                                answerContent.append('<input type="text" name="" class="moreOption" id="addAnswerRadio" placeholder="Thêm tùy chọn" readonly="true">');
                            }
                        });
                        $('#typeAnswerCurrent' + idQuestion).text('Trắc nghiệm');
                    } else if (idTypeQuesion == 4) {
                        type = $(`<div class="answerDropdown"></div>`);
                        $.ajax({
                            url: "/formAPI/forms/part/questions/options/" + idQuestion,
                            dataType: 'json',
                            contentType: "application/json",
                            type: 'GET',
                            async: false,

                            success: function (options) {
                                if (options) {
                                    options.forEach((item, i) => {
                                        let temp = `<div>
                                                                <label class="labelDropdown">
                                                                    <input type="hidden" class="getIdOption" value="` + item.id + `">
                                                                    <span class="countFirstNumberDropMenu"> ` + (i + 1) + `. </span>
                                                                    <input type="text" name="" class="descriptionShortAnswer getOptionIndex" value="` + item.optionQuestion + `" onclick="this.select();" id="getOptionIndex` + i + `">
                                                                    <a class="btnRemove">
                                                                        <img src="/resources/img/form/closeForm.png" alt="">
                                                                    </a>
                                                                </label>
                                                            </div>
                                                            `;
                                        type.append(temp);
                                    });
                                }
                                answerContent.append('<input type="text" name="" class="moreOption" id="addAnswerDropdown" placeholder="Thêm tùy chọn" readonly="true">');
                            }
                        });
                        $('#typeAnswerCurrent' + idQuestion).text('Menu thả');
                    } else if (idTypeQuesion == 5) {
                        type = $(`<div class="answerMulti"></div>`);
                        $.ajax({
                            url: "/formAPI/forms/part/questions/options/" + idQuestion,
                            dataType: 'json',
                            contentType: "application/json",
                            type: 'GET',
                            async: false,

                            success: function (options) {
                                if (options) {
                                    options.forEach((item, i) => {
                                        let temp = `<div>
                                                                <label class="labelDropdown">
                                                                    <input type="checkbox" disabled class="checkboxMulti" name="optradio">
                                                                    <input type="hidden" class="getIdOption" value="` + item.id + `">
                                                                    <input type="text" name="" class="descriptionShortAnswer getOptionIndex" id="getOptionIndex" value="` + item.optionQuestion + `" onclick="this.select();">
                                                                    <a class="btnRemove">
                                                                        <img src="/resources/img/form/closeForm.png" alt="">
                                                                    </a>
                                                                </label>
                                                            </div>
                                                            `;
                                        type.append(temp);
                                    });
                                }
                                answerContent.append('<input type="text" name="" class="moreOption" id="addAnswerCheckbox" placeholder="Thêm tùy chọn" readonly="true">');
                            }
                        });
                        $('#typeAnswerCurrent' + idQuestion).text('Hộp kiểm');
                    } else if (idTypeQuesion == 6) {
                        type = $(`<div class="checkboxMultiGrid"></div>`);
                        let rows = $(`<div class="col-md-5 col-xs-5 cMGRows"></div>`);
                        let cols = $(`<div class="col-md-5 col-xs-5 cMGCols"></div>`);
                        rows.append('<label>Hàng</label>');
                        cols.append('<label>Cột</label>');
                        let rowsContent = $(`<div class="answerCheckboxGridRows"></div>`);
                        let colsContent = $(`<div class="answerChecboxGridCols"></div>`);
                        $.ajax({
                            url: "/formAPI/forms/questions/optionsGrid/rows/" + idQuestion,
                            dataType: 'json',
                            contentType: "application/json",
                            type: 'GET',
                            async: false,

                            success: function (optionRows) {
                                if (optionRows) {
                                    optionRows.forEach((item, i) => {
                                        let temp = `<div>
                                                                <label class="labelDropdown">
                                                                    <input type="hidden" class="getIdOption" value="` + item.id + `">
                                                                    <span class="countAnswerCheckboxGridRows"> ` + (i + 1) + `. </span>
                                                                    <input type="text" name="" class="descriptionShortAnswer getOptionRowsIndex rowS" id="indexOptionRows` + i + `" value="` + item.rows + `" onclick="this.select();">
                                                                    <a class="btnRemove">
                                                                        <img src="/resources/img/form/closeForm.png" alt="">
                                                                    </a>
                                                                </label>
                                                            </div>
                                                            `;
                                        rowsContent.append(temp);
                                    });
                                }
                            },
                        });

                        $.ajax({
                            url: "/formAPI/forms/questions/optionsGrid/cols/" + idQuestion,
                            dataType: 'json',
                            contentType: "application/json",
                            type: 'GET',
                            async: false,

                            success: function (optionCols) {
                                if (optionCols) {
                                    optionCols.forEach((item, i) => {
                                        let temp = `<div>
                                                                <label class="labelDropdown">
                                                                    <input type="checkbox" disabled class="checkboxMulti" name="optradio">
                                                                    <input type="hidden" class="getIdOption" value="` + item.id + `">
                                                                    <input type="text" name="" class="descriptionShortAnswer getOptionColsIndex colS" id="indexOptionCols` + i + `" value="` + item.cols + `" onclick="this.select();">
                                                                    <a class="btnRemove">
                                                                        <img src="/resources/img/form/closeForm.png" alt="">
                                                                    </a>
                                                                </label>
                                                            </div>
                                                            `;
                                        colsContent.append(temp);
                                    });
                                }
                            },
                        });
                        rows.append(rowsContent);
                        rows.append('<input type="text" name="" class="moreOption" id="addAnswerCheckboxGridRows" placeholder="Thêm tùy chọn" readonly="true">');
                        cols.append(colsContent);
                        cols.append('<input type="text" name="" class="moreOption" id="addAnswerChecboxGridCols" placeholder="Thêm tùy chọn" readonly="true">');
                        type.append(rows);
                        type.append(cols);
                        $('#typeAnswerCurrent' + idQuestion).text('Lưới hộp kiểm');
                    } else if (idTypeQuesion == 8) {
                        type = $(`<div class="checkboxMultiGrid"></div>`);
                        let rows = $(`<div class="col-md-5 col-xs-5 cMGRows"></div>`);
                        let cols = $(`<div class="col-md-5 col-xs-5 cMGCols"></div>`);
                        rows.append('<label>Hàng</label>');
                        cols.append('<label>Cột</label>');
                        let rowsContent = $(`<div id="answerRadioGridRows"></div>`);
                        let colsContent = $(`<div id="answerRadioGridCols"></div>`);

                        $.ajax({
                            url: "/formAPI/forms/questions/optionsGrid/rows/" + idQuestion,
                            dataType: 'json',
                            contentType: "application/json",
                            type: 'GET',
                            async: false,

                            success: function (optionRows) {
                                if (optionRows) {
                                    optionRows.forEach((item, i) => {
                                        let temp = `<div>
                                                                <label class="labelDropdown">
                                                                    <input type="hidden" class="getIdOption" value="` + item.id + `">
                                                                    <span class="countAnswerRadioGridRows"> ` + (i + 1) + `. </span>
                                                                    <input type="text" name="" class="descriptionShortAnswer getOptionRowsIndex rowS" id="indexOptionRows` + i + `" value="` + item.rows + `" onclick="this.select();">
                                                                    <a class="btnRemove">
                                                                        <img src="/resources/img/form/closeForm.png" alt="">
                                                                    </a>
                                                                </label>
                                                            </div>
                                                            `;
                                        rowsContent.append(temp);
                                    });
                                }
                            },
                        });
                        $.ajax({
                            url: "/formAPI/forms/questions/optionsGrid/cols/" + idQuestion,
                            dataType: 'json',
                            contentType: "application/json",
                            type: 'GET',
                            async: false,
                            success: function (optionCols) {
                                if (optionCols) {
                                    optionCols.forEach((item, i) => {
                                        let temp = `<div>
                                                                <label class="labelDropdown">
                                                                    <input type="radio" disabled class="checkboxMulti" name="optradio">
                                                                    <input type="hidden" class="getIdOption" value="` + item.id + `">
                                                                    <input type="text" name="" class="descriptionShortAnswer getOptionColsIndex colS" id="indexOptionCols` + i + `" value="` + item.cols + `" onclick="this.select();">
                                                                    <a class="btnRemove">
                                                                        <img src="/resources/img/form/closeForm.png" alt="">
                                                                    </a>
                                                                </label>
                                                            </div>
                                                            `;
                                        colsContent.append(temp);
                                    });
                                }
                            },
                        });

                        rows.append(rowsContent);
                        rows.append('<input type="text" name="" class="moreOption" id="addAnswerRadioGridRows" placeholder="Thêm tùy chọn" readonly="true">');
                        cols.append(colsContent);
                        cols.append('<input type="text" name="" class="moreOption" id="addAnswerRadioGridCols" placeholder="Thêm tùy chọn" readonly="true">');
                        type.append(rows);
                        type.append(cols);
                        $('#typeAnswerCurrent' + idQuestion).text('Lưới trắc nghiệm');
                    } else if (idTypeQuesion == 9) {
                        type = `<div>
                                                <input type="text" name="" class="descriptionShortAnswer" placeholder="Ngày, tháng, năm" readonly="true">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </div>
                                            `;
                        $('#typeAnswerCurrent' + idQuestion).text('Ngày');

                    } else if (idTypeQuesion == 10) {
                        type = `<div>
                                                <input type="text" name="" class="descriptionShortAnswer" placeholder="Giờ" readonly="true">
                                                <span class="iconTimeEdit glyphicon glyphicon-time"></span>
                                            </div>
                                            `;
                        $('#typeAnswerCurrent' + idQuestion).text('Giờ');

                    } else if (idTypeQuesion == 11) {
                        type = `<div>
                                                <input type="text" name="" class="descriptionShortAnswer" placeholder="Chọn thành phố" readonly="true">
                                                <span class="glyphicon glyphicon-chevron-down"></span>
                                            </div>
                                            <div>
                                                <input type="text" name="" class="descriptionShortAnswer" placeholder="Chọn quận/huyện" readonly="true">
                                                <span class="glyphicon glyphicon-chevron-down">
                                                </span>
                                            </div>
                                            <div>
                                                <input type="text" name="" class="descriptionShortAnswer" placeholder="Chọn xã/phường" readonly="true">
                                                <span class="glyphicon glyphicon-chevron-down">
                                                </span>
                                            </div>
                                            `;
                        $('#typeAnswerCurrent' + idQuestion).text('Địa chỉ');

                    }

                    answerContent.append(type);
                });

            }
        });
    }

    var checkChangeForm = 0;
    var textBefore = [];
    var arrIdInput = [];
    $('body').on('click', 'input', function (e) {
        var classInput = $(this).attr('id');
        arrIdInput.push(classInput);
        if (arrIdInput[0] == classInput) {
            textBefore.push($(this).val());
        } else {
            arrIdInput.length = 0;
            textBefore.length = 0;
            textBefore.push($(this).val());
        }
    });


    $('body').on('change', 'input', function () {
        textAfter = $(this).val();
        if (textBefore[0] != textAfter) {
            checkChangeForm = 1;
        }
        if (textBefore[0] == textAfter) {
            checkChangeForm = 0;
        }
    });

    $('body').on('click', '.previewForm', function (e) {
        let countAnser = $('#countAnswers').val();
        if (checkChangeForm == 1 && countAnser == 0) {
            alert("Biểu mẫu đã thay đổi, hãy lưu trước khi muốn xem trước!");
            e.preventDefault();
        }
    });


    $('body').on('click', '.create', function () {
        let countCheckNullQuestion = 0;
        let contentIndexQuestion = [];
        let countCheckOption = 0;
        let contentIndexOption = [];
        $('.titleShortAnswer').each(function (i) {
            if (!($(this).val())) {
                countCheckNullQuestion++;
                contentIndexQuestion.push($(this).attr('id'));
            }
        });
        $('.labelDropdown').find('.descriptionShortAnswer').each(function (i) {
            if (!($(this).val())) {
                countCheckOption++;
                contentIndexOption.push($(this).attr('id'));
            }
        });

        let a = $(this).attr('id');
        let valueCopy = $('.inputTitleCopy').val();
        if (a == 'copyForm') {
            if (!valueCopy) {
                alert('Bạn chưa điền tiêu đề biểu mẫu mới!');
            } else {
                alert('Sao chép biểu mẫu thành công');
                window.location.href = "/home";
            }
        } else if (countCheckNullQuestion > 0) {
            console.log('Hãy điền đầy đủ các câu hỏi!');
        } else if (countCheckOption > 0) {
            console.log('Hãy điền đầy đủ các tùy chọn trong câu hỏi!');
        } else {
            let getTextTitle = $('#title').val();
            if (getTextTitle) {
                let getIdFormToUpdate = $('body').find('#idForm').val();
                $.ajax({
                    url: "/formAPI/forms/" + getIdFormToUpdate,
                    dataType: 'json',
                    contentType: "application/json",
                    type: 'DELETE',

                });
                alert("Cập nhật biểu mẫu thành công!");
            }
        }
    });


    $('body').on('click', '.copyForm', function () {
        $('body').css('overflow', 'auto');
        let getTextTitle = $('#title').val();
        $('.inputTitleCopy').val('Sao chép từ ' + getTextTitle);
    });

    $('body').on('click', '.inputTitleCopy', function () {
        $(this).select();
    });
}

