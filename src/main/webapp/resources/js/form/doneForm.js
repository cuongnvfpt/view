$( document ).ready(function() {
  var idForm = $("#formID").val();
  $.ajax({
    url: "/formAPI/forms/api/" + idForm ,
    dataType: 'json',
    contentType: "application/json",
    type: 'GET',
  }).done(function (result) {
    document.getElementById("title-form").innerHTML=result.title;
  })
});