$(document).ready(function () {
    $(".menu").hide();
    $(".glyphicon-remove").click(function () {
        $(".menu").hide(300);
    });
    $(".burger").click(function () {
        $(".menu").show(300);
    });
    $('.input-group.date').datepicker({format: "dd.mm.yyyy"});

    $('body').on('focusout', '.viewDescriptionShortAnswer', function () {
        $(this).parent().parent().parent().parent().parent().css("border-color", "#144E8C");
        $(this).parent().parent().parent().parent().parent().css("box-shadow", "none");
        let required = $(this).parent().find('.requiredShow').val();
        let value = $(this).val();

        if (required == "true" && !value) {
            $(this).parent().parent().parent().parent().parent().css("border-color", "red");
            $(this).parent().parent().parent().parent().parent().css("box-shadow", "-5px 0px 0px rgba(255, 0, 0, 1)");
        }

    });

    $('body').on('change', '.viewDate', function () {
        $(this).parent().parent().parent().parent().parent().css("border-color", "#144E8C");
        $(this).parent().parent().parent().parent().parent().css("box-shadow", "none");
        let required = $(this).parent().find('.requiredShow').val();
        let value = $(this).val();
        if (required == "true" && !value) {
            $(this).parent().parent().parent().parent().parent().css("border-color", "red");
            $(this).parent().parent().parent().parent().parent().css("box-shadow", "-5px 0px 0px rgba(255, 0, 0, 1)");
        }
    });

    $('body').on('change', '.commune', function () {
        let required = $(this).parent().parent().find('.requiredShow').val();
        let value = $(this).val();

        if (required == "true" && value != -1) {
            $(this).parent().parent().parent().parent().parent().parent().css("border-color", "#144E8C");
            $(this).parent().parent().parent().parent().parent().parent().css("box-shadow", "none");
        }
    });

    $('body').on('click', '.ctnQuestion', function () {
        let type = $(this).find('#typeOfAnswer').val();
        if (type == 2 || type == 4 || type == 5 || type == 6 || type == 8) {
            $(this).css("border-color", "#144E8C");
            $(this).css("box-shadow", "none");
        }
    });

    $('body').on('mouseenter', '.timepicker', function () {
        $(this).mdtimepicker(); //Initializes the time picker
    });

})



