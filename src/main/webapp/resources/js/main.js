$(document).ready(function () {
    $('body').on("click", ".glyphicon-picture", function () {
        let id = $(this).parent().parent().parent().parent().parent().find('.imageQuestion').attr('id');
        idIMG = id;

    });
    $('body').on("click", ".imgCover", function () {
        idIMG = "0";

    });

});
$(document).ready(function () {
    $("#btnSubmit").click(function (event) {
        if ($('#fileInputId2').get(0).files.length === 0) {
            tempAlert1("Bạn chưa chọn ảnh!!");
            return;
        }
        var val1 = $("#fileInputId2").val();
        var val = val1.toLowerCase();
        if (!val.match(/(?:gif|jpg|png|bmp|tiff|jpge)$/)) {
            // inputted file path is not an image of one of the above types
            $("#fileInputId2").val(null);
            tempAlert1("Chọn ảnh và chỉ được phép thêm ảnh!!",3000);
            return;
        }else {
            if($("#fileInputId2")[0].files[0].size > 10000000 && val.match(/(?:gif|jpg|png|bmp|tiff|jpge)$/)) {
                tempAlert1("Ảnh vượt quá 10MB!!",3000);
                $("#fileInputId2").val(null);
                $('#myModal').modal('hide');
            }else {
                event.preventDefault();
                fire_ajax_submit();
            }
        }
    });
});

function fire_ajax_submit() {
    // Get form
    var form = $('#fileUploadForm')[0];
    var data = new FormData(form);
    $("#btnSubmit").prop("disabled", true);
    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        url: "/mediaAPI/api/upload/multi",
        data: data,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        success: function (data) {
            $('#myModal').modal('hide');
            $("#btnSubmit").prop("disabled", false);
            if (idIMG === "0") {
                    $("#img").attr({
                        'src': data,
                    });
                    $("#img").parent().find(".btnRemoveImg").removeAttr("style");
                    $("#img").parent().css('display', 'block');
                    tempAlert1("Thêm ảnh bìa thành công!",3000);

            } else {
                    $("#" + idIMG + "").attr({
                        'src': data,
                    });
                    tempAlert1("Thêm ảnh vào câu hỏi thành công!",3000);
                    $("#" + idIMG + "").parent().css('display', 'block');
                }
            }

    });
}

var idIMG;

var path;

var blCheckClick;

function stups(a) {
    path = a;
    blCheckClick=1;
}

function displayImage() {
    if (idIMG === "0" && blCheckClick===1) {
        $("#img").attr({
            'src': path,
        });
        $("#img").parent().find(".btnRemoveImg").removeAttr("style");
        $("#img").parent().css('display', 'block');
        blCheckClick=0;
    }
    if (idIMG != "0" && blCheckClick===1) {
        $("#" + idIMG + "").attr({
            'src': path,
        });
        $("#" + idIMG + "").parent().css('display', 'block');
        blCheckClick=0;
    }
}


$.ajax({
    url: "/mediaAPI/api/images",
    dataType: 'json',
    contentType: "application/json",
    type: 'GET',
}).done(function (result) {
    result.forEach(item => {
        let divCol3 = $("<div class=\"col-md-3 \"></div>");
        let a = $("<a href=\"javascript:stups('" + item.path + "')\" class=\"out\"></a>");
        let img = $("<img src=\"" + item.path + "\" class=\"img1\">");
        a.append(img);
        divCol3.append(a);
        $("#default_img").append(divCol3);
        $("#default_img1").append(divCol3);
    })
});



function mySubmit(theForm) {
    var val1 = $("#fileInputId").val();
    var val = val1.toLowerCase();
    if (!val.match(/(?:gif|jpg|png|bmp|tiff|jpge)$/)) {
        // inputted file path is not an image of one of the above types
        $("#fileInputId").val(null);
        $('#myModal').modal('hide');
        //$('#myModalUser').modal('hide');
        //tempAlert("Chọn ảnh và chỉ được phép thêm ảnh!!", 3000);
        alert("Chọn ảnh và chỉ được phép thêm ảnh!!");
    }else {
        if($("#fileInputId")[0].files[0].size > 10000000  && val.match(/(?:gif|jpg|png|bmp|tiff|jpge)$/)) {
            tempAlert("Ảnh vượt quá 10MB!!", 3000);
            $("#fileInputId").val(null);
            $('#myModal').modal('hide');
        }else {
            fire_ajax_submit1();
        }
    }
}
function fire_ajax_submit1() {
    // Get form
    var form = $('#fileUploadFormAVT')[0];
    var data = new FormData(form);
    $("#btnSubmitAVT").prop("disabled", true);
    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        url: "/mediaAPI/api/upload/multi",
        data: data,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        success: function (data) {
            console.log("SUCCESS : ", data);
            $("#btnSubmitAVT").prop("disabled", false);
            if (data === "error") {
            } else {
                $("#IMGavatar").attr({
                    'src': data,
                });

            }

        },
    });
}
function tempAlert1(msg) {
    $("#alertContent").html("");
    $("#alertContent").html(msg);
    $("#alert").fadeTo(2000, 500).slideUp(500, function() {
        $("#alert").slideUp(500);
    });
}

// function tempAlert(msg, duration) {
//     var el = document.createElement("div");
//     el.setAttribute("style", "position: fixed;top:60px;right:50%;font-size:14px");
//     el.innerHTML = msg;
//     el.className = "container btn navbar-btn btn-question";
//     setTimeout(function () {
//         el.parentNode.removeChild(el);
//     }, duration);
//     document.body.appendChild(el);
// }

$('#myModal').on('hidden.bs.modal', function (e) {
    $("#fileInputId").val(null);
    $("#fileInputId2").val(null);
});

