package eform.views.service.loginService;


import eform.views.entity.loginEntity.User;

import java.util.Optional;

public interface UserService {

    User findByUsername(String username);

    Optional<User> findByEmail(String email);

    Optional<User> findByResettocken(String resettocken);

    public void save(User user);
}
