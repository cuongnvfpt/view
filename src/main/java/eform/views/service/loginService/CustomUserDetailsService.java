package eform.views.service.loginService;





import eform.views.entity.loginEntity.User;
import eform.views.model.MyUser;
import eform.views.repository.loginRepository.UserRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username);
        if (user != null) {
            List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
            authorities.add(new SimpleGrantedAuthority(user.getFirstlogin().toString()));
            MyUser myUserDetail =
                    new MyUser(
                            user.getUsername(),
                            user.getPassword(),
                            true,
                            true,
                            true,
                            true,
                            authorities);
            myUserDetail.setSetFirstLogin(user.getFirstlogin());
            myUserDetail.setId(user.getId());
            myUserDetail.setSetStatus(user.getStatus());
            BeanUtils.copyProperties(user, myUserDetail);
            return myUserDetail;
        } else {
            throw new UsernameNotFoundException("User not exist with name : " + username);
        }
    }

    @Transactional
    public UserDetails loadUserById(Integer id) {
        User user = userRepository.findById(id).orElseThrow(
                () -> new UsernameNotFoundException("User not found with id : " + id)
        );
        return new CustomUserDetail(user);
    }


}
