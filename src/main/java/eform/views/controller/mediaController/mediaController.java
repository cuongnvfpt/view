package eform.views.controller.mediaController;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class mediaController {
    @RequestMapping(path = "/media", method = RequestMethod.GET)
    public String media() {
        return "media/media";
    }
}
