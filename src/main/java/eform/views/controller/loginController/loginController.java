package eform.views.controller.loginController;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Controller
public class loginController {
    @RequestMapping("/login")
    public String loginPage() {
        return "login/login";
    }
//    @RequestMapping("/logout-success")
//    public String logoutPage()
//    {
//        return "logout";
//    }
    @RequestMapping(value = "/changepassword", method = RequestMethod.GET)
    public String changPassword()
    {
        return "login/changePassword2";
    }

//    @RequestMapping(value = "/login", method = RequestMethod.GET)
//    public ModelAndView Home(HttpServletRequest request) {
//        ModelAndView mav = new ModelAndView("/login/login");
//        String message = request.getParameter("accessDenied");
//        if (message != null) {
//            mav.addObject("message", message);
//        }
//        return mav;
//    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logoutPage() {
        return "login/login";
    }

    // quên mật khẩu gửi vào gmail
    @RequestMapping(value = "/testpassword", method = RequestMethod.GET)
    public String testPassword() {
        return "login/changePassword";
    }

    // đổi mật khẩu khi đăng nhập lần đầu


    @RequestMapping(value = "/reset", method = RequestMethod.GET)
    public ModelAndView displayResetPasswordpage( @RequestParam("token") String token, HttpServletRequest request) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("login/changePassword");
        String uri = "http://localhost:9082/loginAPI/web/checkTocken";
        Map<String,String> data = new HashMap<>();
        data.put("resettocken",token);
        RestTemplate restTemplate = new RestTemplate();
        String result = restTemplate.postForObject( uri, data, String.class);
        if(result.equals(token)){
            modelAndView.addObject("resetTocken", result);
        }else{
            return null;
        }
        System.out.println(result);
         return modelAndView;
    }


}
