package eform.views.controller.homeController;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class homeController {
    @RequestMapping( value = {"/home","/"})
    public String welcome(Model m)
    {
        m.addAttribute("page",0);
        m.addAttribute("typeS",3);
        return "home/home";
    }
    @RequestMapping(path = "address/page/{typeSort}/{page}", method = RequestMethod.GET)
    public String paging(@PathVariable(name = "typeSort") String typeS, @PathVariable(name = "page") String page, Model m, @RequestParam(required = false) String title) {
        try{
            Integer typeSF = Integer.parseInt(typeS);
            Integer pageF = Integer.parseInt(page);

            m.addAttribute("page", pageF);
            m.addAttribute("typeS", typeSF);
            m.addAttribute("title",title);
            return "home/home";
        }catch (NumberFormatException e){
            return "errorPage/404Page";
        }

    }
}
