package eform.views.controller.formController;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class formController {
    @RequestMapping("forms")
    public String createForm() {
        return "form/index";
    }

    @RequestMapping("/forms/edit/{id}")
    public String editForm(Model m, @PathVariable(name = "id") String id) {
        try {
            Integer idF = Integer.parseInt(id);
            m.addAttribute("id", idF);
            return "form/index";
        } catch (NumberFormatException e) {
            return "errorPage/404Page";
        }
    }

    @RequestMapping("public/forms/detail/{id}")
    public String previewForm(Model m, @PathVariable(name = "id") String id) {
        try {
            Integer idF = Integer.parseInt(id);
            m.addAttribute("id", idF);
            return "form/preview";
        } catch (NumberFormatException e) {
            return "errorPage/404Page";
        }
    }

    @RequestMapping("public/forms/complete/{id}")
    public String completeForm(Model m, @PathVariable(name = "id") String id) {
        try {
            Integer idF = Integer.parseInt(id);
            m.addAttribute("id", idF);
            return "form/doneForm";
        } catch (NumberFormatException e) {
            return "errorPage/404Page";
        }
    }

    @RequestMapping("public/errorPage/404")
    public String errorPage() {
        return "errorPage/404Page";
    }

    @RequestMapping("public/errorPage/notReadyUse")
    public String errorPageNotReadyUse() {
        return "errorPage/notReadyUse";
    }

    @RequestMapping("public/errorPage/accessDenied")
    public String errorPageAccessDenied() {
        return "errorPage/accessDenied";
    }

    @RequestMapping("public/formUsing")
    public String formUsing() {
        return "errorPage/usingPage";
    }


}
