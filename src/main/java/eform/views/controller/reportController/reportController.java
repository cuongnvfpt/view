package eform.views.controller.reportController;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class reportController {
    @RequestMapping(path = "/forms/report/{idForm}")
    public String report(Model m, @PathVariable(name="idForm") String id) {
        try {
            Integer idF = Integer.parseInt(id);
            m.addAttribute("id", idF);
            return "report/report";
        } catch (NumberFormatException e) {
            return "errorPage/404Page";
        }
    }
}
