package eform.views.model;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

public class MyUser extends User {
    private Integer id;
    private Boolean setFirstLogin;
    private Boolean setStatus;
    public MyUser(String username, String password, boolean enabled, boolean accountNonExpired,
                  boolean credentialsNonExpired, boolean accountNonLocked,
                  Collection<? extends GrantedAuthority> authorities) {
        super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);

    }

    public Boolean getSetStatus() {
        return setStatus;
    }

    public void setSetStatus(Boolean setStatus) {
        this.setStatus = setStatus;
    }

    public Boolean getSetFirstLogin() {
        return setFirstLogin;
    }

    public void setSetFirstLogin(Boolean setFirstLogin) {
        this.setFirstLogin = setFirstLogin;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
