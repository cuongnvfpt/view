package eform.views.repository.loginRepository;


import eform.views.entity.loginEntity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Integer> {

    User findByUsername(String username);
//    SELECT * FROM user WHERE email = ?

    Optional<User> findByEmail(String email);
//    SELECT * FROM user WHERE reset_token = ?

    Optional<User> findByResettocken(String resettocken);
}
