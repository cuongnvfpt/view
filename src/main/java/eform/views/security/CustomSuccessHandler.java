package eform.views.security;




import eform.views.model.MyUser;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@EnableWebSecurity
public class CustomSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {
    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();
    // private static final Logger log = Logger.getLogger(MySuccessHandler.class);

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
            throws IOException {
        String targetUrl = determineTargetUrl(authentication);
        if (response.isCommitted()) {
            return;
        }
        redirectStrategy.sendRedirect(request, response, targetUrl);
    }

    public String determineTargetUrl(Authentication authentication) {
        MyUser user = (MyUser) (SecurityContextHolder.getContext()).getAuthentication().getPrincipal();
        String url = "";
        if (user.getSetFirstLogin() == false) {
            url = "/changepassword";
        } else if (user.getSetFirstLogin() == true) {
            if(user.getSetStatus() == true) {
                url = "/home";
            }
            else {
                url = "/public/errorPage/accessDenied";
            }
        }

        return url;
    }

    public void setRedirectStrategy(RedirectStrategy redirectStrategy) {
        this.redirectStrategy = redirectStrategy;
    }

    public RedirectStrategy getRedirectStrategy() {
        return redirectStrategy;
    }

    /*private boolean isAdmin(List<String> roles) {
        if (roles.contains(SystemConstant.ROLE_MANAGER)) {
            return true;
        }
        return false;
    }

    private boolean isUser(List<String> roles) {
        if (roles.contains(SystemConstant.ROLE_USER)) {
            return true;
        }
        return false;
    }*/
}

