package eform.views;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class ViewsApplication {

    public static void main(String[] args) {
        SpringApplication.run(ViewsApplication.class, args);
    }
//    @Bean
//    public ModelMapper modelMapper(){
//        return new ModelMapper();
//    }
}
