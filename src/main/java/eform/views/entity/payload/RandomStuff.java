package eform.views.entity.payload;

import lombok.Data;

@Data
public class RandomStuff {
    private String message;

    public String getMessage() {
        return message;
    }

    public RandomStuff(String message) {
        this.message = message;
    }
}
